<?php


namespace Tests\Smorken\DynForm\Integration\Element;


use Smorken\DynForm\Element\Button;
use Smorken\DynForm\Model\Eloquent\Element;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class ButtonTest extends TestCase
{

    use TrimTrait;

    /**
     * @var Button
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new Button();
    }

    public function testRender()
    {
        $e = new Element();
        $e->setName('foo');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<button type="button" name="foo" id="foo" > foo</button>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithElementAttributes()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setElementAttr('class', 'foo');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<button type="button" class="foo" name="foo" id="foo" > foo</button>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithLabel()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setLabel('Foo Label');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<button type="button" name="foo" id="foo" > Foo Label</button>';
        $this->assertEquals($expected, $this->trim($r));
    }
}

