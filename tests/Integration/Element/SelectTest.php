<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 11:03 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element;

use Smorken\DynForm\Element\Select;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class SelectTest extends TestCase
{

    use TrimTrait;

    /**
     * @var Select
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new Select();
    }

    public function testRender()
    {
        $e = $this->makeElement();
        $e->setName('foo');
        $e->setLabel('Foo');
        $e->setData(['foo' => 'bar']);

        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="ctr-foo"> <label for="foo" > Foo </label> <select name="foo" id="foo" >'.
            ' <option value="foo" >bar</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithDefault()
    {
        $e = $this->makeElement();
        $e->setName('foo');
        $e->setLabel('Foo');
        $e->setData(['foo' => 'bar', 'baz' => 'biz']);
        $e->setDefault('baz');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="ctr-foo"> <label for="foo" > Foo </label> <select name="foo" id="foo" >'.
            ' <option value="foo" >bar</option> <option value="baz" selected>biz</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithValueBeforeDefault()
    {
        $e = $this->makeElement();
        $e->setName('foo');
        $e->setLabel('Foo');
        $e->setData(['foo' => 'bar', 'baz' => 'biz']);
        $e->setDefault('baz');
        $r = $this->sut->render($e, $this->app['view'], 'foo');
        $expected = '<div id="ctr-foo"> <label for="foo" > Foo </label> <select name="foo" id="foo" >'.
            ' <option value="foo" selected>bar</option> <option value="baz" >biz</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithValueAfterDefault()
    {
        $e = $this->makeElement();
        $e->setName('foo');
        $e->setLabel('Foo');
        $e->setData(['foo' => 'bar', 'baz' => 'biz']);
        $e->setDefault('foo');
        $r = $this->sut->render($e, $this->app['view'], 'baz');
        $expected = '<div id="ctr-foo"> <label for="foo" > Foo </label> <select name="foo" id="foo" >'.
            ' <option value="foo" >bar</option> <option value="baz" selected>biz</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
