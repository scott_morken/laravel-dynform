<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 11:01 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element;

use Mockery as m;
use Smorken\DynForm\Element\Checkbox;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class CheckBoxTest extends TestCase
{

    use TrimTrait;

    /**
     * @var m/Mock
     */
    protected $element;

    /**
     * @var \Smorken\DynForm\Element\Checkbox
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new Checkbox();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('foo');
        $r = $this->sut->render($element, $this->app['view']);
        $expected = '<div id="ctr-foo"> <input type="checkbox" name="foo" id="foo" > </div>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithCheckbox()
    {
        $element = $this->makeElement();
        $element->setId('foo-1');
        $element->setName('foo');
        $element->setLabel('Foo Label');
        $r = $this->sut->render($element, $this->app['view']);
        $expected = '<div id="ctr-foo-1"> <input type="checkbox" id="foo-1" name="foo" ><label for="foo-1" > Foo Label </label> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithHiddenValue()
    {
        $element = $this->makeElement();
        $element->setId('foo-1');
        $element->setName('foo');
        $element->setHiddenValue(0);
        $r = $this->sut->render($element, $this->app['view']);
        $expected = '<div id="ctr-foo-1"> <input type="hidden" name="foo" value="0" > <input type="checkbox" id="foo-1" name="foo" > </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderCheckedWithHiddenValue()
    {
        $element = $this->makeElement();
        $element->setName('foo');
        $element->setValue(1);
        $element->setChecked(true);
        $element->setHiddenValue(0);
        $r = $this->sut->render($element, $this->app['view']);
        $expected = '<div id="ctr-foo-1"> <input type="hidden" name="foo" value="0" > <input type="checkbox" id="foo-1"'.
            ' name="foo" value="1" checked="checked" > </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
