<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:58 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element;

use Smorken\DynForm\Element\CheckboxMulti;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class CheckBoxMultiTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\CheckboxMulti
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new CheckboxMulti();
    }

    public function testDefaultChecks()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(2);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" ><label for="cb-1" > Foo </label> </div>'.
            ' <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" checked="checked" >'.
            '<label for="cb-2" > Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testMultiCheckedWithDefaultBefore()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar', '3' => 'Fiz']);
        $element->setDefault(1);
        $view = $this->app['view'];
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" ><label for="cb-1" > Foo </label> </div>'.
            ' <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" checked="checked" >'.
            '<label for="cb-2" > Bar </label> </div> <div class="checkable"> <input type="checkbox" id="cb-3"'.
            ' name="cb[]" value="3" checked="checked" ><label for="cb-3" > Fiz </label> </div> </div> </div>';
        $r = $this->sut->render($element, $view, [2, 3]);
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testMultiCheckedWithDefaultMid()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar', '3' => 'Fiz']);
        $element->setDefault(2);
        $view = $this->app['view'];
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" checked="checked" ><label for="cb-1" > Foo </label>'.
            ' </div> <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" ><label for="cb-2" >'.
            ' Bar </label> </div> <div class="checkable"> <input type="checkbox" id="cb-3" name="cb[]" value="3"'.
            ' checked="checked" ><label for="cb-3" > Fiz </label> </div> </div> </div>';
        $r = $this->sut->render($element, $view, [1, 3]);
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testMultiChecks()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, [1, 2]);
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" checked="checked" ><label for="cb-1" > Foo </label>'.
            ' </div> <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" checked="checked" >'.
            '<label for="cb-2" > Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testOneChecked()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" checked="checked" ><label for="cb-1" > Foo </label>'.
            ' </div> <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" ><label for="cb-2" >'.
            ' Bar </label> </div> </div> </div>';
        $r = $this->sut->render($element, $view, 1);
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testOneCheckedWithDefaultAfter()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(2);
        $view = $this->app['view'];
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" checked="checked" ><label for="cb-1" > Foo </label>'.
            ' </div> <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" >'.
            '<label for="cb-2" > Bar </label> </div> </div> </div>';
        $r = $this->sut->render($element, $view, 1);
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testOneCheckedWithDefaultBefore()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(1);
        $view = $this->app['view'];
        $expected = '<div id="ctr-cb"> <label for="cb" > Checkbox </label> <div> <div class="checkable">'.
            ' <input type="checkbox" id="cb-1" name="cb[]" value="1" ><label for="cb-1" > Foo </label>'.
            ' </div> <div class="checkable"> <input type="checkbox" id="cb-2" name="cb[]" value="2" checked="checked" >'.
            '<label for="cb-2" > Bar </label> </div> </div> </div>';
        $r = $this->sut->render($element, $view, 2);
        $this->assertEquals($expected, $this->trim($r));
    }
}
