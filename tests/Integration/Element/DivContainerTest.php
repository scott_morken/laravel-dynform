<?php


namespace Tests\Smorken\DynForm\Integration\Element;


use Smorken\DynForm\Element\Div;
use Smorken\DynForm\Model\Eloquent\Element;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class DivContainerTest extends TestCase
{

    use TrimTrait;

    /**
     * @var Div
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new Div();
    }

    public function testRender()
    {
        $e = new Element();
        $e->setName('foo');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="foo" ></div>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithElementAttributes()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setElementAttr('class', 'foo');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div class="foo" id="foo" ></div>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithHelp()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setLabel('Foo Label');
        $e->setHelp('help help');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="foo" > <div class="label">Foo Label</div> <div class="help">help help</div></div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithLabel()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setLabel('Foo Label');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="foo" > <div class="label">Foo Label</div></div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
