<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 11:12 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Checkbox;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class CheckBoxTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Checkbox
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Checkbox();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-cb" class="form-check mb-2"> <input type="checkbox" class="form-check-input" name="cb"'.
            ' id="cb" value="1" ><label class="form-check-label" for="cb" > Checkbox </label> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithChecked()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setChecked(true);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-cb" class="form-check mb-2"> <input type="checkbox" class="form-check-input" name="cb"'.
            ' id="cb" value="1" checked="checked" ><label class="form-check-label" for="cb" > Checkbox </label> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithHiddenValue()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setHiddenValue(0);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-cb-1" class="form-check mb-2"> <input type="hidden" name="cb" value="0" >'.
            ' <input type="checkbox" id="cb-1" class="form-check-input" name="cb" value="1" ><label class="form-check-label"'.
            ' for="cb-1" > Checkbox </label> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
