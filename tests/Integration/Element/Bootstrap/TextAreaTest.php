<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:29 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\TextArea;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class TextAreaTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\TextArea
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new TextArea();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('TA');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > TA </label>'.
            ' <textarea class="form-control" name="ele" id="ele" >1</textarea> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithExtraAttributes()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('TA');
        $element->setElementAttributes(['rows' => 3, 'cols' => 50]);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > TA </label> <textarea rows="3"'.
            ' cols="50" class="form-control" name="ele" id="ele" >1</textarea> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
