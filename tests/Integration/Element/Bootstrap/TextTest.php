<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:28 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Text;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class TextTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Text
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Text();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Text');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Text </label> <input type="text"'.
            ' class="form-control" name="ele" id="ele" value="1" > </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
