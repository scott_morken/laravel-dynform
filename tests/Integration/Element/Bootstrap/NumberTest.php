<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:19 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Number;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class NumberTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Number
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Number();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Number');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Number </label> <input type="number"'.
            ' class="form-control" name="ele" id="ele" value="1" > </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
