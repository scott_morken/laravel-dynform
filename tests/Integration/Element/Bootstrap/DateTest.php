<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:13 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Date;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class DateTest extends TestCase
{
    
    use TrimTrait;
    
    /**
     * @var \Smorken\DynForm\Element\Date
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Date();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Date');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Date </label> <div class="input-group date"'.
            ' id="datetimepicker-ele" data-target-input="nearest"> <input type="text" class="form-control datetimepicker-input"'.
            ' data-target="#datetimepicker-ele" name="ele" id="ele" > <div class="input-group-append"'.
            ' data-target="#datetimepicker-ele" data-toggle="datetimepicker"> <div class="input-group-text">'.
            '<i class="fas fa-calendar"></i></div> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
