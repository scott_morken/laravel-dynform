<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:22 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Select;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class SelectTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Select
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Select();
    }

    public function testRenderNoSelection()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Select');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Select </label> <select class="form-control"'.
            ' name="ele" id="ele" > <option value="1" >Foo</option> <option value="2" >Bar</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithSelection()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Select');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Select </label> <select class="form-control"'.
            ' name="ele" id="ele" > <option value="1" selected>Foo</option> <option value="2" >Bar</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderNoValue()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Select');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setValue(2);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Select </label> <select class="form-control"'.
            ' name="ele" id="ele" > <option value="1" >Foo</option> <option value="2" selected>Bar</option> </select> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
