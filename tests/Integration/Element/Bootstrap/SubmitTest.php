<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:26 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Submit;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class SubmitTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Submit
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Submit();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Submit');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<button type="submit" class="btn" name="ele" id="ele" > Submit</button>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithClass()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Submit');
        $element->setElementAttr('class', 'btn-success');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<button type="submit" class="btn-success btn" name="ele" id="ele" > Submit</button>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
