<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:10 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Button;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class ButtonTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\CheckboxMulti
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Button();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Button');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<button type="button" class="btn" name="ele" id="ele" > Button</button>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderWithClass()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Button');
        $element->setElementAttr('class', 'btn-secondary');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<button type="button" class="btn-secondary btn" name="ele" id="ele" > Button</button>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
