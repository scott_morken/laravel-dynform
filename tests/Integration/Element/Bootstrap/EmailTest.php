<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 12:16 PM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\Email;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class EmailTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Email
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new Email();
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('ele');
        $element->setLabel('Email');
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 'email@example.org');
        $expected = '<div id="ctr-ele" class="form-group mb-2"> <label for="ele" > Email </label>'.
            ' <input type="email" class="form-control" name="ele" id="ele" value="email@example.org" > </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
