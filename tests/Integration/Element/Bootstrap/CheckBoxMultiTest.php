<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 11:55 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element\Bootstrap;

use Smorken\DynForm\Element\CheckboxMulti;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class CheckBoxMultiTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\CheckboxMulti
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::bootstrap');
        $this->sut = new CheckboxMulti();
    }

    public function testDefaultChecks()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(2);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<div id="ctr-cb" class="mb-2"> <label class="form-check-label" for="cb" > Checkbox </label> <div>'.
            ' <div class="form-check"> <input type="checkbox" id="cb-1" class="form-check-input" name="cb[]" value="1" >'.
            '<label for="cb-1" class="form-check-label" > Foo </label> </div> <div class="form-check">'.
            ' <input type="checkbox" id="cb-2" class="form-check-input" name="cb[]" value="2" checked="checked" >'.
            '<label for="cb-2" class="form-check-label" > Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testMultiChecks()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, [1, 2]);
        $expected = '<div id="ctr-cb" class="mb-2"> <label class="form-check-label" for="cb" > Checkbox </label> <div>'.
            ' <div class="form-check"> <input type="checkbox" id="cb-1" class="form-check-input" name="cb[]" value="1"'.
            ' checked="checked" ><label for="cb-1" class="form-check-label" > Foo </label> </div> <div class="form-check">'.
            ' <input type="checkbox" id="cb-2" class="form-check-input" name="cb[]" value="2" checked="checked" >'.
            '<label for="cb-2" class="form-check-label" > Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRender()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<div id="ctr-cb" class="mb-2"> <label class="form-check-label" for="cb" > Checkbox </label> <div>'.
            ' <div class="form-check"> <input type="checkbox" id="cb-1" class="form-check-input" name="cb[]" value="1" >'.
            '<label for="cb-1" class="form-check-label" > Foo </label> </div> <div class="form-check">'.
            ' <input type="checkbox" id="cb-2" class="form-check-input" name="cb[]" value="2" >'.
            '<label for="cb-2" class="form-check-label" > Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderChecksValue()
    {
        $element = $this->makeElement();
        $element->setName('cb');
        $element->setLabel('Checkbox');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, 1);
        $expected = '<div id="ctr-cb" class="mb-2"> <label class="form-check-label" for="cb" > Checkbox </label> <div>'.
            ' <div class="form-check"> <input type="checkbox" id="cb-1" class="form-check-input" name="cb[]" value="1" checked="checked" >'.
            '<label for="cb-1" class="form-check-label" > Foo </label> </div> <div class="form-check">'.
            ' <input type="checkbox" id="cb-2" class="form-check-input" name="cb[]" value="2" ><label for="cb-2" class="form-check-label" >'.
            ' Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
