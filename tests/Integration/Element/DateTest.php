<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 11:05 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element;

use Smorken\DynForm\Element\Date;
use Smorken\DynForm\Model\Eloquent\Element;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class DateTest extends TestCase
{

    use TrimTrait;

    /**
     * @var Date
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new Date();
    }

    public function testRender()
    {
        $e = new Element();
        $e->setName('foo');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="ctr-foo"> <input type="text" class="date" name="foo" id="foo" > </div>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithElementAttributes()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setElementAttr('class', 'foo');
        $e->setElementAttr('placeholder', 'YYYY-MM-DD');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="ctr-foo"> <input type="text" class="foo date" placeholder="YYYY-MM-DD" name="foo" id="foo" > </div>';
        $this->assertEquals(
            $expected,
            $this->trim($r)
        );
    }

    public function testRenderWithLabel()
    {
        $e = new Element();
        $e->setName('foo');
        $e->setLabel('Foo Label');
        $r = $this->sut->render($e, $this->app['view']);
        $expected = '<div id="ctr-foo"> <label for="foo" > Foo Label </label> <input type="text" class="date" name="foo" id="foo" > </div>';
        $this->assertEquals($expected, $this->trim($r));
    }
}
