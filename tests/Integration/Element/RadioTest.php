<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 11:02 AM
 */

namespace Tests\Smorken\DynForm\Integration\Element;

use Smorken\DynForm\Element\Radio;
use Tests\Smorken\DynForm\Integration\TrimTrait;
use Tests\Smorken\DynForm\TestCase;

class RadioTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\Element\Radio
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $this->sut = new Radio();
    }

    public function testDefaultChecks()
    {
        $element = $this->makeElement();
        $element->setName('r');
        $element->setLabel('Radio');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(2);
        $view = $this->app['view'];
        $r = $this->sut->render($element, $view, null);
        $expected = '<div id="ctr-r"> <label for="r" > Radio </label> <div> <div class="checkable">'.
            ' <input type="radio" id="r-1" name="r" value="1" ><label for="r-1" > Foo </label> </div>'.
            ' <div class="checkable"> <input type="radio" id="r-2" name="r" value="2" checked="checked" >'.
            '<label for="r-2" > Bar </label> </div> </div> </div>';
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderValueOverridesDefaultAfter()
    {
        $element = $this->makeElement();
        $element->setName('r');
        $element->setLabel('Radio');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(1);
        $view = $this->app['view'];
        $expected = '<div id="ctr-r"> <label for="r" > Radio </label> <div> <div class="checkable">'.
            ' <input type="radio" id="r-1" name="r" value="1" ><label for="r-1" > Foo </label> </div>'.
            ' <div class="checkable"> <input type="radio" id="r-2" name="r" value="2" checked="checked" >'.
            '<label for="r-2" > Bar </label> </div> </div> </div>';
        $r = $this->sut->render($element, $view, 2);
        $this->assertEquals($expected, $this->trim($r));
    }

    public function testRenderValueOverridesDefaultBefore()
    {
        $element = $this->makeElement();
        $element->setName('r');
        $element->setLabel('Radio');
        $element->setData(['1' => 'Foo', '2' => 'Bar']);
        $element->setDefault(2);
        $view = $this->app['view'];
        $expected = '<div id="ctr-r"> <label for="r" > Radio </label> <div> <div class="checkable">'.
            ' <input type="radio" id="r-1" name="r" value="1" checked="checked" ><label for="r-1" > Foo </label> </div>'.
            ' <div class="checkable"> <input type="radio" id="r-2" name="r" value="2" ><label for="r-2" > Bar </label>'.
            ' </div> </div> </div>';
        $r = $this->sut->render($element, $view, 1);
        $this->assertEquals($expected, $this->trim($r));
    }
}
