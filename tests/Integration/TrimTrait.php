<?php


namespace Tests\Smorken\DynForm\Integration;


trait TrimTrait
{

    protected function trim($string)
    {
        return preg_replace('/\s+/', ' ', preg_replace('/\r\n|\r|\n/', '', $string));
    }
}
