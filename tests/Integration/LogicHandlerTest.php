<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:56 AM
 */

namespace Tests\Smorken\DynForm\Integration;

use Illuminate\Support\Facades\App;
use Smorken\DynForm\Contracts\Storage\Logic;
use Smorken\DynForm\LogicHandler;
use Tests\Smorken\DynForm\TestCase;

class LogicHandlerTest extends TestCase
{

    /**
     * @var \Smorken\DynForm\LogicHandler
     */
    protected $sut;

    protected $view;

    public function setUp(): void
    {
        parent::setUp();
        $this->view = App::make('view');
        $lp = App::make(Logic::class);
        $this->sut = new LogicHandler($lp);
    }

    public function testSingleElementNoLogic()
    {
        $element = $this->makeElement();
        $element->setName('foo');
        $element->setLabel('Foo');
        $this->sut->addElement($element);
        $this->assertNull($this->sut->createLogicView($this->view));
    }

    public function testSingleElementSingleLogic()
    {
        $element = $this->makeElement();
        $element->setName('foo');
        $element->setLabel('Foo');
        $logic = new \Smorken\DynForm\Model\VO\Logic();
        $logic->action = 'show';
        $logic->comparator = '=';
        $logic->element = 'bar';
        $logic->part = 'value';
        $logic->value = 'fiz';
        $element->logic = $logic;
        $this->sut->addElement($element);
        $contains = "$('form').on('change', smorken.dynform.logic.getElement('bar')";
        $this->assertStringContainsString($contains,
            $this->sut->createLogicView($this->view)
                      ->render()
        );
    }
}
