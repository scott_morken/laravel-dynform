<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:37 AM
 */

namespace Tests\Smorken\DynForm\Integration;

use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\DynForm\Contracts\Handler\Logic;
use Smorken\DynForm\Contracts\Storage\Builder;
use Smorken\DynForm\FormHandler;
use Smorken\DynForm\Model\Eloquent\Form;
use Smorken\DynForm\Model\VO\Element;
use Smorken\DynForm\Model\VO\Option;
use Smorken\DynForm\Model\VO\Options;
use Smorken\Support\Arr;
use Tests\Smorken\DynForm\TestCase;

class FormHandlerTest extends TestCase
{

    use TrimTrait;

    /**
     * @var \Smorken\DynForm\FormHandler
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('dynform.view_base', 'smorken/dynform::simple');
        $bi = $this->app->make(Builder::class);
        $this->sut = new FormHandler(
            $this->app['view'], $bi, $this->app[Logic::class], 'smorken/dynform::simple.container.form'
        );
    }

    public function teardown(): void
    {
        m::close();
    }

    public function testFormCreate()
    {
        $form = new Form();
        $this->sut->create($form, []);
        $expected = '<form method="post" accept-charset="UTF-8" ></form>';
        $this->assertEquals($expected, $this->trim($this->sut->render()));
    }

    public function testFormCreateRaw()
    {
        $form = new Form();
        $this->sut->create($form, []);
        $r =(Arr::stringify($this->sut->raw()));
        $this->assertStringContainsString('0: Form ', $r);
        $this->assertStringContainsString('1: Form ', $r);
    }

    public function testFormCreateSimple()
    {
        $csrf = $this->makeElement();
        $csrf->setBuilder('csrf');
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setBuilder('text');
        $ele->setValue(1);
        $form = $ele->form;
        $form->setRelation('elements', new Collection([$csrf, $ele]));
        $this->sut->create($form, []);
        $expected = '<form method="post" accept-charset="UTF-8" ><input type="hidden" name="_token" value=""><div id="ctr-foo-ele">'.
            ' <input type="text" name="foo-ele" id="foo-ele" value="1" > </div></form>';
        $this->assertEquals($expected, $this->trim($this->sut->render()));
    }

    public function testFormCreateSimpleRaw()
    {
        $csrf = $this->makeElement();
        $csrf->setBuilder('csrf');
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setBuilder('text');
        $ele->setValue(1);
        $form = $ele->form;
        $form->elements = new Collection([$csrf, $ele]);
        $this->sut->create($form, []);
        $r =(Arr::stringify($this->sut->raw()));
        $this->assertStringContainsString('0: Form ', $r);
        $this->assertStringContainsString('1: # element-', $r);
        $this->assertStringContainsString('2: # foo-ele (text) [0]', $r);
        $this->assertStringContainsString('3: Form ', $r);
    }

    public function testFormCreateSimpleAndFailValidation()
    {
        $val = new \Smorken\DynForm\Model\VO\Validator('required', 'Pick me!');
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setBuilder('text');
        $ele->setValidators([$val]);
        $form = $ele->form;
        $form->elements = new Collection([$ele]);
        $this->sut->create($form, ['baz-ele' => 'foo']);
        $expected = '<form method="post" accept-charset="UTF-8" ><div id="ctr-foo-ele">'.
            ' <input type="text" name="foo-ele" id="foo-ele" > <div class="text-error">Pick me!</div> </div></form>';
        $this->assertCount(1, $this->sut->getErrors());
        $this->assertEquals($expected, $this->trim($this->sut->render()));
    }

    public function testFormCreateSimpleContainer()
    {
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setLabel('foo-label');
        $ele->setId('div-1');
        $ele->setBuilder('div');
        $ele->is_container = true;
        $form = $ele->form;
        $form->elements = new Collection([$ele]);
        $this->sut->create($form, []);
        $expected = '<form method="post" accept-charset="UTF-8" ><div id="div-1" >'.
            ' <div class="label">foo-label</div></div></form>';
        $this->assertEquals($expected, $this->trim($this->sut->render()));
    }

    public function testFormCreateSimpleContainerRaw()
    {
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setLabel('foo-label');
        $ele->setId('div-1');
        $ele->setBuilder('div');
        $ele->is_container = true;
        $form = $ele->form;
        $form->elements = new Collection([$ele]);
        $this->sut->create($form, []);
        $r =(Arr::stringify($this->sut->raw()));
        $this->assertStringContainsString('0: Form ', $r);
        $this->assertStringContainsString('1: foo-label: # foo-ele (div) [0] (container)', $r);
        $this->assertStringContainsString('2: foo-label: # foo-ele (div) [0] (container)', $r);
        $this->assertStringContainsString('3: Form ', $r);
    }

    public function testFormCreateSimpleWithElementAttributes()
    {
        $o = new Option('attributes', 'class', 'foo');
        $os = new Options($o);
        $container = new Collection();
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setBuilder('text');
        $ele->setValue(1);
        $ele->setElementAttributes($os->getOption('attributes'));
        $container->push($ele);
        $form = $ele->form;
        $form->elements = $container;
        $this->sut->create($form, []);
        $expected = '<form method="post" accept-charset="UTF-8" ><div id="ctr-foo-ele">'.
            ' <input type="text" class="foo" name="foo-ele" id="foo-ele" value="1" > </div></form>';
        $this->assertEquals($expected, $this->trim($this->sut->render()));
    }

    public function testFormCreateWithOptions()
    {
        $o = new Option('attributes', 'method', 'get');
        $form = new Form();
        $form->setOptions(new Options($o));
        $this->sut->create($form, []);
        $expected = '<form method="get" accept-charset="UTF-8" ></form>';
        $this->assertEquals($expected, $this->trim($this->sut->render()));
    }

    public function testFormValidateAndFailValidation()
    {
        $val = new \Smorken\DynForm\Model\VO\Validator('required');
        $container = new Collection();
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setBuilder('text');
        $ele->setValidators([$val]);
        $container->push($ele);
        $form = $ele->form;
        $form->elements = $container;
        $this->assertFalse($this->sut->validate($form, ['baz-ele' => 'foo']));
    }

    public function testFormValidateAndPassValidation()
    {
        $val = new \Smorken\DynForm\Model\VO\Validator('required');
        $container = new Collection();
        $ele = $this->makeElement();
        $ele->setName('foo-ele');
        $ele->setBuilder('text');
        $ele->setValidators([$val]);
        $container->push($ele);
        $form = $ele->form;
        $form->elements = $container;
        $this->assertTrue($this->sut->validate($form, ['foo-ele' => 'foo']));
    }
}
