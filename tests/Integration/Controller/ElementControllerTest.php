<?php


namespace Tests\Smorken\DynForm\Integration\Controller;


use Smorken\DynForm\Model\Eloquent\Element;
use Smorken\DynForm\Model\Eloquent\Form;
use Tests\Smorken\DynForm\DBTestCase;

class ElementControllerTest extends DBTestCase
{

    public function testBaseRoute()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/element')
             ->see('Element Administration')
             ->see('No records found');
    }

    public function testCreateSuccessfully()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/element')
             ->click('New')
             ->see('Create new record')
             ->type('Test Element', 'name')
             ->press('Save')
             ->seePageIs('/admin/dynform/element')
             ->see('TestElement');
    }

    public function testCreateSuccessfullyWithOption()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/element')
             ->click('New')
             ->see('Create new record');
        $form = $this->getForm('Save');
        $values = $form->getValues();
        $input = [
            '_token' => $values['_token'],
            'name' => 'Test Element',
            'builder' => 'select',
            'option' => [
                'key' => [
                    0 => 'attributes',
                ],
                'item' => [
                    0 => 'class',
                ],
                'value' => [
                    0 => 'fiz-buz',
                ],
            ],
            'validator' => [
                'validators' => [
                    0 => 'required',
                ],
                'message' => [
                    0 => '',
                ],
            ],
            'data' => [
                'key' => [
                    0 => 'foo',
                    1 => 'bar',
                ],
                'value' => [
                    0 => 'Foo Value',
                    1 => 'Bar Value',
                ],
            ],
        ];
        $this->makeRequest($form->getMethod(), $form->getUri(), $input)
             ->seePageIs('/admin/dynform/element')
             ->see('TestElement')
             ->visit('/admin/dynform/element/view/1')
             ->see('<div>attributes class fiz-buz</div>')
             ->see('<div>required</div>')
             ->see('<div>foo: Foo Value</div>')
             ->see('<div>bar: Bar Value</div>');
    }

    public function testDeleteFailsWhenElementHasForms()
    {
        $form = $this->formCreation();
        $e = $this->createElement($form,
            [
                'id' => 1, 'name' => 'div-row', 'is_container' => 1, 'builder' => 'div', 'label' => 'Foo Label',
                'help' => 'Help text...',
            ], ['weight' => 1]);
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/element')
             ->see($e->getName())
             ->click('delete')
             ->seePageIs('/admin/dynform/element')
             ->see($e->getName())
             ->see('Element #1 belongs to one or more forms and cannot be deleted');
    }

    public function testDeleteSuccessfully()
    {
        $m = factory(Element::class)->create();
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/element')
             ->see($m->getName())
             ->click('delete')
             ->see('Delete record')
             ->press('Delete')
             ->seePageIs('/admin/dynform/element?page=1')
             ->dontSee($m->getName());
    }

    protected function createElement($form, $attributes, $pivot = [])
    {
        $e = factory(Element::class)->create($attributes);
        $e->forms()->attach($form->id, $pivot);
        return $e;
    }

    protected function formCreation()
    {
        return factory(Form::class)->create();
    }
}
