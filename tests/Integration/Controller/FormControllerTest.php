<?php


namespace Tests\Smorken\DynForm\Integration\Controller;


use Smorken\DynForm\Model\Eloquent\Element;
use Smorken\DynForm\Model\Eloquent\Entry;
use Smorken\DynForm\Model\Eloquent\Form;
use Tests\Smorken\DynForm\DBTestCase;
use Tests\Smorken\DynForm\Integration\TrimTrait;

class FormControllerTest extends DBTestCase
{

    use TrimTrait;

    public function testBaseRoute()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/form')
             ->see('Form Administration')
             ->see('No records found');
    }

    public function testComplexCreationWithPreview()
    {
        $form = $this->formCreation();
        $div_row = $this->createElement($form,
            [
                'id' => 1, 'name' => 'div-row', 'is_container' => 1, 'builder' => 'div', 'label' => 'Foo Label',
                'help' => 'Help text...',
            ], ['weight' => 1]);
        $div_col_1 = $this->createElement($form,
            ['id' => 2, 'name' => 'div-col-1', 'is_container' => 1, 'builder' => 'div', 'label' => '', 'help' => ''],
            ['container_id' => 1, 'weight' => 2]);
        $div_col_2 = $this->createElement($form,
            ['id' => 3, 'name' => 'div-col-2', 'is_container' => 1, 'builder' => 'div', 'label' => '', 'help' => ''],
            ['container_id' => 1, 'weight' => 3]);
        $fname = $this->createElement($form,
            [
                'id' => 5, 'name' => 'first_name', 'builder' => 'text', 'label' => 'First Name', 'default' => '',
                'help' => '',
            ],
            ['container_id' => 2, 'weight' => 4]);
        $lname = $this->createElement($form,
            [
                'id' => 6, 'name' => 'last_name', 'builder' => 'text', 'label' => 'Last Name', 'default' => '',
                'help' => 'Your last name',
            ],
            ['container_id' => 3, 'weight' => 5]);
        $submit = $this->createElement($form, ['id' => 7, 'name' => 'submit', 'builder' => 'submit',], ['weight' => 6]);
        $form_source = '<form method="post" accept-charset="UTF-8" ><div id="div-row" >'.
            ' <div class="label">Foo Label</div> <div class="help">Help text...</div>'.
            '<div id="div-col-1" ><div id="ctr-first-name" class="form-group mb-2"> <label for="first-name" > First Name </label>'.
            ' <input type="text" class="form-control" name="first_name" id="first-name" > </div></div>'.
            '<div id="div-col-2" ><div id="ctr-last-name" class="form-group mb-2"> <label for="last-name" > Last Name </label>'.
            ' <input type="text" class="form-control" name="last_name" id="last-name" >'.
            ' <div class="form-text text-muted"><small>Your last name</small></div></div></div></div>'.
            '<button type="submit" class="btn" name="submit" id="submit" > '.$submit->getLabel().'</button></form>';
        $response = $this->actingAs($this->mockAuth()[0])
                         ->visit('/admin/dynform/form')
                         ->see($form->getName())
                         ->click('preview')
            ->response->getContent();
        $this->assertStringContainsString($form_source, $this->trim($response));
    }

    public function testCreateSuccessfully()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/form')
             ->click('New')
             ->seePageIs('/admin/dynform/form/create')
             ->see('Create new record')
             ->type('Test Form', 'name')
             ->press('Save')
             ->seePageIs('/admin/dynform/form')
             ->see('Test Form');
    }

    public function testCreateSuccessfullyWithOption()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/form')
             ->click('New')
             ->seePageIs('/admin/dynform/form/create')
             ->see('Create new record');
        $form = $this->getForm('Save');
        $values = $form->getValues();
        $input = [
            '_token' => $values['_token'],
            'name' => 'Test Form',
            'option' => [
                'key' => [
                    0 => 'attributes',
                ],
                'item' => [
                    0 => 'class',
                ],
                'value' => [
                    0 => 'fiz-buz',
                ],
            ],
        ];
        $this->makeRequest($form->getMethod(), $form->getUri(), $input)
             ->seePageIs('/admin/dynform/form')
             ->see('Test Form')
             ->see('attributes class fiz-buz');
    }

    public function testCreateWithValidationErrors()
    {
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/form')
             ->click('New')
             ->seePageIs('/admin/dynform/form/create')
             ->see('Create new record')
             ->press('Save')
             ->seePageIs('/admin/dynform/form/create')
             ->see('name field is required');
    }

    public function testDeleteFailsWhenFormHasEntries()
    {
        $m = factory(Form::class)->create();
        $e = factory(Entry::class)->create(['form_id' => $m->id]);
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/form')
             ->see($m->getName())
             ->click('delete')
             ->seePageIs('/admin/dynform/form')
             ->see($m->getName())
             ->see('Form #1 has one or more entries and cannot be deleted');
    }

    public function testDeleteSuccessfully()
    {
        $m = factory(Form::class)->create();
        $this->actingAs($this->mockAuth()[0])
             ->visit('/admin/dynform/form')
             ->see($m->getName())
             ->click('delete')
             ->seePageIs('/admin/dynform/form/delete/'.$m->id)
             ->see('Delete record')
             ->press('Delete')
             ->seePageIs('/admin/dynform/form')
             ->dontSee($m->getName());
    }

    protected function createElement($form, $attributes, $pivot = [])
    {
        $e = factory(Element::class)->create($attributes);
        $e->forms()->attach($form->id, $pivot);
        return $e;
    }

    protected function formCreation()
    {
        return factory(Form::class)->create();
    }
}
