<?php

namespace Tests\Smorken\DynForm;

use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class DBTestCase extends TestCase
{

    use DatabaseMigrations;

    protected $defineGates = true;

    protected function seedApp()
    {
        $this->seed('RoleSeeder');
        $this->seed('TestSeeder');
    }
}
