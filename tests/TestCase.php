<?php namespace Tests\Smorken\DynForm;

use Mockery as m;
use Smorken\CacheAssist\ServiceProvider;
use Smorken\DynForm\Model\Eloquent\Element;
use Smorken\DynForm\Model\Eloquent\Form;
use Smorken\Roles\Models\Eloquent\RoleUser;

abstract class TestCase extends \Orchestra\Testbench\BrowserKit\TestCase
{

    protected $baseUrl = 'http://localhost';

    protected $defineGates = false;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedApp();
        $this->defineGates($this->app);
    }

    protected function defineGates($app)
    {
        if ($this->defineGates) {
            \Smorken\Roles\ServiceProvider::defineGates($app, true);
        }
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set(
            'database.connections.testbench',
            [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]
        );
        $app['config']->set('dynform.view_master', 'smorken/dynform::layouts.master');
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\DynForm\ServiceProvider::class,
            \Smorken\Ext\Controller\ServiceProvider::class,
            \Smorken\Roles\ServiceProvider::class,
            ServiceProvider::class,
        ];
    }

    protected function makeElement(\Smorken\DynForm\Contracts\Model\Form $form = null)
    {
        if (is_null($form)) {
            $form = new Form();
        }
        $e = new Element();
        $e->setRelation('form', $form);
        return $e;
    }

    protected function mockAuth($user_data = ['id' => 1, 'first_name' => 'foo', 'last_name' => 'bar'], $admin = true)
    {
        $user = m::mock('Illuminate\Contracts\Auth\Authenticatable');
        foreach ($user_data as $k => $v) {
            $user->$k = $v;
        }
        if ($admin) {
            RoleUser::create(['user_id' => $user->id, 'role_id' => 1]);
        }
//        $user->shouldReceive('getAuthIdentifier')->andReturn($user_data['id']);
//        $user->shouldReceive('getLogin');
        $this->be($user);
        return [$user];
    }

    protected function mockElement($name, $label)
    {
        $r = m::mock('Illuminate\Database\Eloquent\Relations\Relation');
        $f = m::mock('Smorken\DynForm\Model\InterfaceForm');
        $r->shouldReceive('getResults')
          ->andReturn($f);
        $e = $this->mock('\Smorken\DynForm\Model\Eloquent\Element')
                  ->makePartial();
        $e->shouldReceive('form')
          ->andReturn($r);
        $e->setName($name);
        $e->setLabel($label);
        return $e;
    }

    protected function mockIt($class)
    {
        $mock = m::mock($class);
        $ioc_binding = preg_replace('/\[.*\]/', '', $class);
        $this->app->instance($ioc_binding, $mock);

        return $mock;
    }

    protected function seedApp()
    {
    }
}
