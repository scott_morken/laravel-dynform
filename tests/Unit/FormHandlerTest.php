<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:08 AM
 */

namespace Tests\Smorken\DynForm\Unit;

use Illuminate\Contracts\View\Factory;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\DynForm\Contracts\Handler\Logic;
use Smorken\DynForm\Contracts\Storage\Builder;
use Smorken\DynForm\FormHandler;

class FormHandlerTest extends TestCase
{

    /**
     * @var FormHandler
     */
    protected $sut;

    public function setUp(): void
    {
        $view = m::mock(Factory::class);
        $builder = m::mock(Builder::class);
        $logic = m::mock(Logic::class);
        $this->sut = new FormHandler(
            $view, $builder, $logic, 'smorken/dynform::simple.container.form'
        );
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testWalkOutputRecursive()
    {
        $arr = ['foo', ['bar']];
        $this->assertEquals('foobar', $this->sut->walkOutput($arr));
    }

    public function testWalkOutputRecursiveComplex()
    {
        $arr = [
            'foo',
            ['bar', ['biz'], 'baz'],
            ['buz'],
            'fiz',
        ];
        $this->assertEquals('foobarbizbazbuzfiz', $this->sut->walkOutput($arr));
    }

    public function testWalkOutputSingleLevel()
    {
        $arr = ['foo', 'bar'];
        $this->assertEquals('foobar', $this->sut->walkOutput($arr));
    }

    public function testWalkOutputWithCollection()
    {
        $c = new \Illuminate\Support\Collection(['foo', 'bar']);
        $this->assertEquals('foobar', $this->sut->walkOutput($c));
    }

    public function testWalkOutputWithEmptyCollection()
    {
        $c = new \Illuminate\Support\Collection();
        $this->assertEquals('', $this->sut->walkOutput($c));
    }
}
