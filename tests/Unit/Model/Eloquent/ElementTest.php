<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:26 AM
 */

namespace Tests\Smorken\DynForm\Unit\Model\Eloquent;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\DynForm\Model\Eloquent\Element;

class ElementTest extends TestCase
{

    /**
     * @var \Smorken\DynForm\Model\Eloquent\Element
     */
    protected $sut;

    public function setUp(): void
    {
        $this->sut = m::mock(Element::class)->makePartial();
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testGetElementAttributes()
    {
        $this->assertEmpty($this->sut->getElementAttributes());
    }

    public function testGetElementAttributesAsString()
    {
        $this->sut->setElementAttributes(['foo' => 'bar']);
        $this->assertEquals('foo="bar"', $this->sut->getElementAttributesAsString());
    }

    public function testGetElementAttributesAsStringWithAdditional()
    {
        $this->sut->setElementAttributes(['foo' => 'bar']);
        $this->assertEquals('foo="bar" baz="biz"', $this->sut->getElementAttributesAsString(['baz' => 'biz']));
    }

    public function testGetElementAttributesExcept()
    {
        $this->sut = new Element();
        $this->sut->setElementAttributes(['foo' => 'bar']);
        $this->sut->setName('fiz');
        $this->sut->setValue('1');
        $this->sut->setChecked(true);
        $expected = [
            'foo' => 'bar',
            'name' => 'fiz',
            'id' => 'fiz',
        ];
        $this->assertEquals($expected, $this->sut->getElementAttributesExcept(['checked', 'value', 'bar']));
    }

    public function testGetElementAttributesIsSet()
    {
        $this->sut->setElementAttributes(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $this->sut->getElementAttributes());
    }

    public function testGetIdBySetIdOverridesName()
    {
        $this->sut->setName('foo');
        $this->sut->setId('bar');
        $this->assertEquals('bar', $this->sut->getId());
    }

    public function testGetIdFromComplexName()
    {
        $this->sut->setName('item[user_id][test_id]');
        $this->assertEquals('item-user-id-test-id', $this->sut->getId());
    }

    public function testGetIdSimpleByName()
    {
        $this->sut->setName('foo');
        $this->assertEquals('foo', $this->sut->getId());
    }

    public function testGetIdSimpleWithAppend()
    {
        $this->sut->setName('foo');
        $this->assertEquals('foo-bar', $this->sut->getId('bar'));
    }

    public function testGetLabelAttributes()
    {
        $this->assertEmpty($this->sut->getLabelAttributes());
    }

    public function testGetLabelAttributesAsString()
    {
        $this->sut->setLabelAttributes(['foo' => 'bar']);
        $this->assertEquals('foo="bar"', $this->sut->getLabelAttributesAsString());
    }

    public function testGetLabelAttributesAsStringWithAdditional()
    {
        $this->sut->setLabelAttributes(['foo' => 'bar']);
        $this->assertEquals('foo="bar" baz="biz"', $this->sut->getLabelAttributesAsString(['baz' => 'biz']));
    }

    public function testGetLabelAttributesIsSet()
    {
        $this->sut->setLabelAttributes(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $this->sut->getLabelAttributes());
    }

    public function testGetNameRemovesInvalidCharacters()
    {
        $this->sut->setName('i!tem-1[user_id]^%()[test_id]');
        $this->assertEquals('item-1[user_id][test_id]', $this->sut->getName());
    }

    public function testGetNameWhenEmptyIsRandom()
    {
        $this->sut = new Element();
        $this->assertStringStartsWith('element-', $this->sut->getName());
    }

    public function testGetValue()
    {
        $this->assertNull($this->sut->getValue());
    }

    public function testGetValueWithDefault()
    {
        $this->sut->setDefault('foo');
        $this->assertEquals('foo', $this->sut->getValue());
    }

    public function testGetValueWithDefaultOverridesDefault()
    {
        $this->sut->setDefault('foo');
        $this->sut->setValue('bar');
        $this->assertEquals('bar', $this->sut->getValue());
    }

    public function testGetValueWithValue()
    {
        $this->sut->setValue('foo');
        $this->assertEquals('foo', $this->sut->getValue());
    }
}
