<?php


namespace Tests\Smorken\DynForm\Unit\Model\Eloquent;

use Illuminate\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\DynForm\Contracts\Storage\Builder;
use Smorken\DynForm\Model\Eloquent\Element;
use Smorken\DynForm\Model\Eloquent\Form;
use Smorken\Support\Arr;

class FormTest extends TestCase
{

    /**
     * @var Application
     */
    protected $app;

    public function setUp(): void
    {
        parent::setUp();
        $this->app = Container::getInstance();
        $this->app->bind(Builder::class, function ($c) { return new \Smorken\DynForm\Storage\Config\Builder([], []); });
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testGetStackedCreatesDefaultContainerElement()
    {
        $sut = new Form();
        $sut->elements = new Collection();
        $r = $sut->getStacked();
        $this->assertCount(1, $r);
        $this->assertEquals(0, $r->first()->id);
    }

    public function testGetStackedWithComplexContainers()
    {
        $div_row = $this->makeElement(['id' => 1, 'name' => 'div-row', 'is_container' => 1, 'builder' => 'div', 'weight' => 1]);
        $div_col_1 = $this->makeElement(['id' => 2, 'name' => 'div-col-1', 'is_container' => 1, 'builder' => 'div', 'container_id' => 1, 'weight' => 2]);
        $div_col_2 = $this->makeElement(['id' => 3, 'name' => 'div-col-2', 'is_container' => 1, 'builder' => 'div', 'container_id' => 1, 'weight' => 3]);
        $csrf = $this->makeElement(['id' => 4, 'name' => 'csrf', 'builder' => 'csrf', 'weight' => 0]);
        $fname = $this->makeElement(['id' => 5, 'name' => 'first_name', 'builder' => 'text', 'label' => 'First Name', 'container_id' => 2, 'weight' => 4]);
        $lname = $this->makeElement(['id' => 6, 'name' => 'last_name', 'builder' => 'text', 'label' => 'Last Name', 'container_id' => 3, 'weight' => 5]);
        $submit = $this->makeElement(['id' => 7, 'name' => 'submit', 'builder' => 'submit', 'weight' => 6]);
        $elements = (new Collection([$div_row, $div_col_1, $div_col_2, $csrf, $fname, $lname, $submit]))->sortBy('weight');
        $sut = new Form();
        $sut->elements = $elements;
        $r = $sut->getStacked();
        $zero = $r->first();
        $zchildren = $zero->getChildren();
        $this->assertCount(3, $zchildren);
        $this->assertEquals($csrf, $zchildren[0]);
        $this->assertEquals($div_row, $zchildren[1]);
        $this->assertEquals($submit, $zchildren[2]);
        $rowchildren = $zchildren[1]->getChildren();
        $this->assertCount(2, $rowchildren);
        $this->assertEquals($div_col_1, $rowchildren[0]);
        $this->assertEquals($div_col_2, $rowchildren[1]);
        $c1 = $rowchildren[0]->getChildren();
        $this->assertCount(1, $c1);
        $this->assertEquals($fname, $c1[0]);
        $c2 = $rowchildren[1]->getChildren();
        $this->assertCount(1, $c2);
        $this->assertEquals($lname, $c2[0]);
    }

    protected function makeElement(array $attributes)
    {
        $ele = new ElementStub();
        $ele->forceFill($attributes);
        $ele->setApp($this->app);
        return $ele;
    }
}

class ElementStub extends Element
{

}
