<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:25 AM
 */

namespace Tests\Smorken\DynForm\Unit\Model\VO;

use PHPUnit\Framework\TestCase;
use Smorken\DynForm\Model\VO\Option;
use Smorken\DynForm\Model\VO\Options;

class OptionsTest extends TestCase
{
    public function testComplex()
    {
        $o = [];
        $o[] = new Option('method', null, 'post');
        $o[] = new Option('url', null, 'foo.com');
        $o[] = new Option('attributes', 'class', 'foo');
        $o[] = new Option('attributes', 'id', 'foo-id');
        $o[] = new Option('attributes', 'ph', 'foo-ph');
        $os = new Options();
        $os->addOptions($o);
        $expected = [
            'method' => 'post',
            'url' => 'foo.com',
            'attributes' => [
                'class' => 'foo',
                'id' => 'foo-id',
                'ph' => 'foo-ph',
            ],
        ];
        $this->assertEquals($expected, $os->getOptions());
        $this->assertEquals($expected['attributes'], $os->getOption('attributes'));
        $this->assertEquals($expected['attributes']['class'], $os->getOption('attributes.class'));
    }

    public function testSimple()
    {
        $o = new Option('method', null, 'post');
        $os = new Options();
        $os->addOption($o);
        $this->assertEquals(['method' => 'post'], $os->getOptions());
    }

    public function testSimpleWithItem()
    {
        $o = new Option('attributes', 'class', 'foo');
        $os = new Options();
        $os->addOption($o);
        $expected = [
            'attributes' => [
                'class' => 'foo',
            ],
        ];
        $this->assertEquals($expected, $os->getOptions());
    }

    public function testSingleDepth()
    {
        $o = [];
        $o[] = new Option('label_attributes', 'foo', 'bar');
        $os = new Options();
        $os->addOptions($o);
        $expected = [
            'label_attributes' => [
                'foo' => 'bar',
            ],
        ];
        $this->assertEquals($expected, $os->getOptions());
    }
}
