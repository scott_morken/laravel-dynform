<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:19 AM
 */

namespace Tests\Smorken\DynForm\Unit {

    use PHPUnit\Framework\TestCase;
    use Smorken\DynForm\LookupHandler;

    class LookupHandlerTest extends TestCase
    {
        /**
         * @var LookupHandler
         */
        protected $sut;

        public function setUp(): void
        {
            $this->sut = new LookupHandler();
        }

        public function testRun()
        {
            $lookup = 'cls:Tests\Smorken\DynForm\Unit\Runner;new:true;method:foo';
            $r = $this->sut->run($lookup);
            $this->assertEquals('bar', $r);
        }

        public function testRunStatic()
        {
            $lookup = 'cls:\Tests\Smorken\DynForm\Unit\RunnerStatic;new:false;method:foo';
            $r = $this->sut->run($lookup);
            $this->assertEquals('fiz', $r);
        }

        public function testRunWithoutClsReturnsErrors()
        {
            $lookup = "cls:;new:true;method:fiz;params:bar,baz";
            $r = $this->sut->run($lookup);
            $this->assertEquals(['cls is a required lookup key.'], $r);
        }

        public function testRunWithparams()
        {
            $lookup = "cls:Tests\Smorken\DynForm\Unit\Runner;new:true;method:fiz;params:bar,baz";
            $r = $this->sut->run($lookup);
            $this->assertEquals('barbaz', $r);
        }
    }

    class Runner
    {

        public function fiz($bar, $baz)
        {
            return $bar.$baz;
        }

        public function foo()
        {
            return 'bar';
        }
    }

    class RunnerStatic
    {

        public static function foo()
        {
            return 'fiz';
        }
    }
}
