<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 10:34 AM
 */

namespace Tests\Smorken\DynForm\Unit\Element;

use Illuminate\Contracts\View\Factory;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\DynForm\Contracts\Model\Element;
use Smorken\DynForm\Element\Text;
use Smorken\DynForm\Model\VO\Options;

class TextTest extends TestCase
{

    /**
     * @var m|Element
     */
    protected $model;

    /**
     * @var \Smorken\DynForm\Element\Text
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = m::mock(Element::class);
        $this->sut = new Text();
        $this->sut->setViewName('simple');
        $this->sut->setViewBase('view-foo');
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testRender()
    {
        $v = m::mock(Factory::class);
        $v->shouldReceive('make')
          ->with('view-foo.text.simple', m::type('array'))
          ->andReturn($v);
        $v->shouldReceive('exists')
          ->with('view-foo.text.simple')
          ->andReturn(true);
        $v->shouldReceive('render')
          ->andReturn('foo');
        $this->model->shouldReceive('setValue')
                    ->with('bar');
        $this->model->shouldReceive('getOptions')
                    ->andReturn(new Options());
        $this->model->shouldReceive('getOption')
                    ->with('view')
                    ->andReturn(false);
        $this->assertEquals('foo', $this->sut->render($this->model, $v, 'bar'));
    }
}
