<div id="ctr-{{ $element->getId() }}" class="form-group mb-2">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    @include('smorken/dynform::_input._select', ['items' => $element->getData(), 'attrs' => $element->getElementAttributesExcept(['checked', 'value'], ['class' => 'form-control'])])
    @include('smorken/dynform::bootstrap.partials.standard_block')
</div>
