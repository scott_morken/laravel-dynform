<div id="ctr-{{ $element->getId() }}" class="mb-2">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes(['class' => 'form-check-label'])])
    <div>
        @foreach($element->getMultiElements() as $melement)
            <div class="form-check">
                @include('smorken/dynform::bootstrap.checkbox._checkbox', ['element' => $melement])
            </div>
        @endforeach
    </div>
    @include('smorken/dynform::bootstrap.partials.standard_block')
</div>
