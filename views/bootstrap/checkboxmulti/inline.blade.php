<div id="ctr-{{ $element->getId() }}" class="mb-2">
    <label {{ $element->getLabelAttributesAsString() }}>
        {{ $element->getLabel() }}
    </label>
    @foreach($element->getMultiElements() as $melement)
        <div class="form-check form-check-inline">
            @include('smorken/dynform::bootstrap.checkbox._checkbox', ['element' => $melement])
        </div>
    @endforeach
    @include('smorken/dynform::bootstrap.partials.standard_block')
</div>
