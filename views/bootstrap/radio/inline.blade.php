<div id="ctr-{{ $element->getId() }}" class="mb-2">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes(['class' => 'form-check-label'])])
    @foreach($element->getMultiElements() as $melement)
        <div class="form-check form-check-inline">
            @include('smorken/dynform::bootstrap.radio._radio', ['element' => $melement])
        </div>
    @endforeach
    @include('smorken/dynform::bootstrap.partials.standard_block')
</div>
