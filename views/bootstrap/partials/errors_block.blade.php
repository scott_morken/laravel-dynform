@if (isset($builder) && $builder->getErrors()->has($element->getName()))
    @foreach($builder->getErrors()->get($element->getName()) as $error)
        <div class="text-danger">{{ $error }}</div>
    @endforeach
@endif
