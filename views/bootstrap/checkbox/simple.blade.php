<div id="ctr-{{ $element->getId() }}" class="form-check mb-2">
    @if ($element->hiddenValue() !== false)
        @include('smorken/dynform::_input._input', ['type' => 'hidden', 'attrs' => ['name' => $element->getName(), 'value' => $element->hiddenValue()]])
    @endif
    @include('smorken/dynform::bootstrap.checkbox._checkbox')
    @include('smorken/dynform::bootstrap.partials.standard_block')
</div>
