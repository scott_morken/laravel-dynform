@include('smorken/dynform::_input._input', ['type' => 'checkbox', 'attrs' => $element->getAllElementAttributes(['class' => 'form-check-input'])])
@include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes(['class' => 'form-check-label'])])
