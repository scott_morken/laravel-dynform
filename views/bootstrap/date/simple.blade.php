<div id="ctr-{{ $element->getId() }}" class="form-group mb-2">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    <div class="input-group date" id="datetimepicker-{{ $element->getId() }}" data-target-input="nearest">
        @include('smorken/dynform::_input._input', ['attrs' => $element->getElementAttributesExcept(['checked'], ['class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker-'.$element->getId()])])
        <div class="input-group-append" data-target="#datetimepicker-{{ $element->getId() }}"
             data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fas fa-calendar"></i></div>
        </div>
    </div>
    @include('smorken/dynform::simple.partials.standard_block')
</div>
