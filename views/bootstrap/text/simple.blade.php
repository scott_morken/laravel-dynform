<div id="ctr-{{ $element->getId() }}" class="form-group mb-2">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    @include('smorken/dynform::_input._input', ['type' => 'text', 'attrs' => $element->getElementAttributesExcept(['checked'], ['class' => 'form-control'])])
    @include('smorken/dynform::bootstrap.partials.standard_block')
</div>
