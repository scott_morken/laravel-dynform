@include('smorken/dynform::_input._input', ['type' => 'checkbox', 'attrs' => $element->getAllElementAttributes()])
@include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
