<div id="ctr-{{ $element->getId() }}">
    @if ($element->hiddenValue() !== false)
        @include('smorken/dynform::_input._input', ['type' => 'hidden', 'attrs' => ['name' => $element->getName(), 'value' => $element->hiddenValue()]])
    @endif
    @include('smorken/dynform::simple.checkbox._checkbox')
    @include('smorken/dynform::simple.partials.standard_block')
</div>
