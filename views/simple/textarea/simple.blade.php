<div id="ctr-{{ $element->getId() }}">
    <div>
        @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    </div>
    @include('smorken/dynform::_input._textarea', ['attrs' => $element->getElementAttributesExcept(['checked', 'value'])])
    @include('smorken/dynform::simple.partials.standard_block')
</div>
