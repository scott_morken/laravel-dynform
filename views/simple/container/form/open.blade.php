@php
    $attrs = [
        'method' => 'post',
        'accept-charset' => 'UTF-8'
    ];
    $attrs = array_replace($attrs, $element->getOptions()->toArray()['attributes'] ?? []);
    $method = strtolower($attrs['method'] ?? 'POST');
@endphp
@include('smorken/dynform::simple.container._open', ['container' => 'form', 'attrs' => $attrs])
