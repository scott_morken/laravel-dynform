@include('smorken/dynform::simple.container._open', ['container' => 'div', 'attrs' => $element->getElementAttributesExcept(['checked', 'value', 'name'])])
@if ($element->getLabel())
    <div class="label">{{ $element->getLabel() }}</div>
@endif
@if ($element->getHelp())
    <div class="help">{{ $element->getHelp() }}</div>
@endif
