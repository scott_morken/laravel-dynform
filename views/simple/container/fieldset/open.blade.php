@include('smorken/dynform::simple.container._open', ['container' => 'fieldset', 'attrs' => $element->getElementAttributesExcept(['checked', 'value'])])
@if ($element->getLabel())
    <legend>{{ $element->getLabel() }}</legend>
@endif
@if ($element->getHelp())
    <div class="help">{{ $element->getHelp() }}</div>
@endif
