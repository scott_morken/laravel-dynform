<div id="ctr-{{ $element->getId() }}">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    <div>
        @foreach($element->getMultiElements() as $melement)
            <div class="checkable">
                @include('smorken/dynform::simple.radio._radio', ['element' => $melement])
            </div>
        @endforeach
    </div>
    @include('smorken/dynform::simple.partials.standard_block')
</div>
