<div id="ctr-{{ $element->getId() }}">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    @include('smorken/dynform::_input._input', ['attrs' => $element->getElementAttributesExcept(['checked'], ['class' => 'date'])])
    @include('smorken/dynform::simple.partials.standard_block')
</div>
