<div id="ctr-{{ $element->getId() }}">
    @include('smorken/dynform::_input._label', ['attrs' => $element->getLabelAttributes()])
    @include('smorken/dynform::_input._select', ['items' => $element->getData(), 'attrs' => $element->getElementAttributesExcept(['checked', 'value'])])
    @include('smorken/dynform::simple.partials.standard_block')
</div>
