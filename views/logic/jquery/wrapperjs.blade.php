<script>
    $(document).ready(function () {
        var smorken = smorken || {};
        smorken.dynform = smorken.dynform || {};
        smorken.dynform.logic = {
            init: function () {
                $.each(smorken.dynform.logic.listeners, function (i, f) {
                    f();
                });
            },
            listeners: [
                function () {
                    @foreach($logics as $elementname => $logicarray)
                    @foreach($logicarray as $element_id => $logic)
                    $('form').on('change', smorken.dynform.logic.getElement('{{ $elementname }}'), function (e) {
                        smorken.dynform.logic.handleLogic(smorken.dynform.logic.getElement('{{ $elementname }}'), '{{ $element_id }}', '{{ $logic->action }}', '{{ $logic->part }}', '{{ $logic->comparator }}', '{{ $logic->value }}')
                    });
                    smorken.dynform.logic.handleLogic(smorken.dynform.logic.getElement('{{ $elementname }}'), '{{ $element_id }}', '{{ $logic->action }}', '{{ $logic->part }}', '{{ $logic->comparator }}', '{{ $logic->value }}')
                    @endforeach
                    @endforeach
                }
            ],
            handleLogic: function (compare_element, element_id, action, part, comparator, compare_to) {
                var element_value = smorken.dynform.logic.getValue(part, compare_element);
                if (smorken.dynform.logic.check(element_value, comparator, compare_to)) {
                    smorken.dynform.logic.doAction(element_id, action);
                }
                else {
                    smorken.dynform.logic.doElseAction(element_id, action);
                }
            },
            check: function (value, comparator, compare_to) {
                switch (comparator) {
                    case '=':
                        return value === compare_to;
                    case '<>':
                        return value !== compare_to;
                    case '<':
                        return value < compare_to;
                    case '<=':
                        return value <= compare_to;
                    case '>':
                        return value > compare_to;
                    case '>=':
                        return value >= compare_to;
                }
            },
            getElement: function (name) {
                var ele = $('#' + name);
                if (ele.length) {
                    return ele;
                }
                ele = $("input[name='" + name + "']");
                return ele;
            },
            getValue: function (part, selector) {
                var value = '';
                switch (part) {
                    case 'value':
                        value = $(selector).val();
                        break;
                }
                return value;
            },
            doAction: function (element_id, action) {
                switch (action) {
                    case 'show':
                        $('#ctr-' + element_id).show();
                        break;
                    case 'hide':
                        $('#ctr-' + element_id).hide();
                        break;
                }
            },
            doElseAction: function (element_id, current_action) {
                switch (current_action) {
                    case 'show':
                        smorken.dynform.logic.doAction(element_id, 'hide');
                        break;
                    case 'hide':
                        smorken.dynform.logic.doAction(element_id, 'show');
                        break;
                }
            }
        };
        smorken.dynform.logic.init();
    });
</script>
