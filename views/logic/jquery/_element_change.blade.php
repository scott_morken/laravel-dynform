@foreach($logicarray as $element_id => $logic)
    var element_value = dynform.smorken.logic.getValue('{{ $logic->part }}', namestring);
    var comparator = '{{ $logic->comparator }}';
    var comp_value = '{{ $logic->value }}';
    var action = '{{ $logic->action }}';
    if (dynform.smorken.logic.check(element_value, comparator, comp_value)) {
    dynform.smorken.logic.doAction('{{ $element_id }}', action);
    }
    else {
    dynform.smorken.logic.doElseAction('{{ $element_id }}', action);
    }
@endforeach
