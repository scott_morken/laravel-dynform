<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter)?$filter->all():[]);
$inputs_view = $inputs_view ?? '';
?>
@section('content')
    @include('smorken/dynform::_preset.controller._to_index')
    @include('smorken/dynform::_preset.controller._title', ['title' => $title??''])
    <h5 class="mb-2">Update record [{{ $model->getKey() }}]</h5>
    <form method="post" action="{{ action([$controller, 'doSave'], $params) }}">
        @csrf
        <input type="hidden" name="_id" value="{{ $model->getKey() }}">
        <div class="mb-4">
            @includeFirst([$inputs_view, 'smorken/dynform::_preset.controller._not_found'])
        </div>
        <div class="">
            <button type="submit" name="save" class="btn btn-primary">Save</button>
            <a href="{{ action([$controller, 'index'], $params) }}" title="Cancel" class="btn btn-outline-secondary">Cancel</a>
        </div>
    </form>
@endsection
