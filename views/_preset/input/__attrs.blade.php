<?php
$attrs = $attrs ?? [];
?>
@foreach($attrs as $attr_key => $attr_value)
    @include('smorken/dynform::_preset.input.__attr')
@endforeach
