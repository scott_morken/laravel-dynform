<div class="form-check {{ $wrapper_classes??'' }}">
    @include('smorken/dynform::_preset.input._checkbox')
    @include('smorken/dynform::_preset.input._label', ['id' => ($id ?? $name).'-'.($value ?? 1), 'label_classes' => 'form-check-label'])
</div>
