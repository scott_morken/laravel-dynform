<a @if (isset($id)) id="{{ $id }}" @endif
        @include('smorken/dynform::_preset.input.__attrs', ['attrs' => ['href' => $href ?? '#', 'title' => $title ?? '', 'class' => $classes ?? '']])
        @include('smorken/dynform::_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>
    {{ $title ?? 'link' }}
</a>
