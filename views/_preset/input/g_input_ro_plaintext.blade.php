<div class="form-group {{ $wrapper_classes??'' }}">
    @include('smorken/dynform::_preset.input._label')
    @include('smorken/dynform::_preset.input._input_ro_plaintext')
</div>
