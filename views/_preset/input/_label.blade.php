@php
    $skip_error = $skip_error ?? false;
    $name = $name ?? 'input-unknown-'.rand(0, 1000);
@endphp
<label @include('smorken/dynform::_preset.input.__id', ['attr_name' => 'for']) class="{{ $label_classes ?? '' }}"
        @include('smorken/dynform::_preset.input.__attrs', ['attrs' => $add_attrs ?? []])>
    {{ $title ?? $name }}
    @if ($skip_error === false && isset($errors) && $errors->has($name))
    &middot; <small class="text-danger">{{ $name }}</small>
    @endif
</label>

