@php
    $type = (!isset($type) || !is_string($type) ? 'text' : $type);
    $name = $name ?? 'input-'.$type.'-'.rand(0, 1000);
    $attrs = [
        'attrs' => [
            'type' => $type,
            'class' => 'form-control-plaintext '.($classes ?? ''),
            'name' => $name,
            'value' => $value ?? old($name, (isset($model) && strlen($model->$name)) ? $model->$name : null),
            'placeholder' => $placeholder ?? false,
            'maxlength' => $maxlength ?? 255,
            'disabled' => null,
        ]
    ];
@endphp
<input @include('smorken/dynform::_preset.input.__id')
        @include('smorken/dynform::_preset.input.__attrs', $attrs)
        @include('smorken/dynform::_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>
