@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@include('smorken/dynform::_preset.controller.view', [
    'title' => 'Element Administration',
    'limit_columns' => ['id', 'name', 'label', 'builder', 'default', 'container', 'is_container', 'weight']
    ])
@section('content')
    <div class="mb-2">
        <h3>Options</h3>
        @forelse ($model->getOptions()->getOptions(false) as $o)
            <div>{{ $o }}</div>
        @empty
            <div class="text-muted">None</div>
        @endforelse
    </div>
    <div class="mb-2">
        <h3>Validators</h3>
        @forelse ($model->validators as $o)
            <div>{{ $o }}</div>
        @empty
            <div class="text-muted">None</div>
        @endforelse
    </div>
    <div class="mb-2">
        <h3>Data</h3>
        @forelse ($model->data as $k => $v)
            <div>{{ $k }}: {{ $v }}</div>
        @empty
            <div class="text-muted">None</div>
        @endforelse
    </div>
    <div class="mb-2">
        <h3>Forms</h3>
        @forelse ($model->forms as $f)
            <div>
                <a href="{{ action([config('dynform.forms.controller'), 'view'], ['id' => $f->id]) }}"
                   title="View form {{ $f->name }}">
                    {{ $f->name }}
                </a>
            </div>
        @empty
            <div class="text-muted">Not assigned.</div>
        @endforelse
    </div>
@append
