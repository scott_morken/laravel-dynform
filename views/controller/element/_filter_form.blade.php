<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                @include('smorken/dynform::_preset.input._label', ['name' => 'builder', 'title' => 'Builder', 'label_classes' => 'sr-only'])
                @include('smorken/dynform::_preset.input._select', [
                'name' => 'builder',
                'classes' => strlen($filter->builder) ? $filtered : '',
                'value' => $filter->builder,
                'items' => $builders ?? []
                ])
            </div>
            <div class="form-group mb-2 mr-2">
                @include('smorken/dynform::_preset.input._label', ['name' => 'name', 'title' => 'Name', 'label_classes' => 'sr-only'])
                @include('smorken/dynform::_preset.input._input', [
                'name' => 'name',
                'classes' => $filter->name ? $filtered : '',
                'value' => $filter->name,
                'placeholder' => 'Name'
                ])
            </div>
            <div class="form-group mb-2 mr-2">
                @include('smorken/dynform::_preset.input._label', ['name' => 'label', 'title' => 'Label', 'label_classes' => 'sr-only'])
                @include('smorken/dynform::_preset.input._input', [
                'name' => 'label',
                'classes' => $filter->label ? $filtered : '',
                'value' => $filter->label,
                'placeholder' => 'Label'
                ])
            </div>
            <div class="form-group mb-2 mr-2">
                @include('smorken/dynform::_preset.input._label', ['name' => 'is_container', 'title' => 'Is Container', 'label_classes' => 'sr-only'])
                @include('smorken/dynform::_preset.input._select', [
                'name' => 'is_container',
                'classes' => strlen($filter->is_container) ? $filtered : '',
                'value' => $filter->is_container,
                'items' => ['' => 'Any container type', '0' => 'Not a container', '1' => 'Is a container']
                ])
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
