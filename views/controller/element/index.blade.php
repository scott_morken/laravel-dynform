@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@section('content')
    @include('smorken/dynform::_preset.controller._title', ['title' => 'Element Administration'])
    @include('smorken/dynform::controller.element._filter_form')
    @include('smorken/dynform::_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Builder</th>
                <th>Name</th>
                <th>Label</th>
                <th>Container</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>
                        @include('smorken/dynform::_preset.controller.index_actions._view', ['value' => $model->getKey()])
                    </td>
                    <td>{{ $model->builder }}</td>
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->label }}</td>
                    <td>{{ $model->is_container ? 'Yes' : 'No' }}</td>
                    <td>
                        @include('smorken/dynform::_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
