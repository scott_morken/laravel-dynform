<div>
    These represent "list" items. The key is the computer friendly side, the message is the label or what is presented
    to the user.
</div>
@if (isset($lookups) && count($lookups))
    <div>
        <strong>Lookups:</strong>
        <small>These provide server backed data lookups</small>
        <div><strong>Key:</strong> php</div>
        @foreach($lookups as $value => $name)
            <div>
                <strong>{{ $name }}:</strong> {{ $value }}
            </div>
        @endforeach
    </div>
@else
    <div class="text-muted">No lookups available.</div>
@endif
