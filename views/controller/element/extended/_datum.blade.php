<div class="data row form-group">
    <div class="col-5">
        <input type="text" name="data[key][]" value="{{ $key }}" placeholder="key" class="form-control">
    </div>
    <div class="col-6">
        <input type="text" name="data[value][]" value="{{ $content }}" placeholder="message" class="form-control">
    </div>
    <div class="col-1">
        <button type="button" class="close">
            <span aria-hidden="true" class="text-danger form-text">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
</div>
