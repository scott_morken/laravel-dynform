<div class="validator row form-group">
    <div class="col-5">
        @include('smorken/dynform::_preset.input._input', [
        'name' => 'validator[validators][]',
        'value' => $content ? $content->validators : null,
        'placeholder' => 'validators'
        ])
    </div>
    <div class="col-6">
        @include('smorken/dynform::_preset.input._input', [
        'name' => 'validator[message][]',
        'value' => $content ? $content->message : null,
        'placeholder' => 'message'
        ])
    </div>
    <div class="col-1">
        <button type="button" class="close">
            <span aria-hidden="true" class="text-danger form-text">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
</div>
