<div class="logic row form-group">
    <div class="col-2">
        @include('smorken/dynform::_preset.input._select', [
        'name' => 'logic[action]',
        'items' => $logic->getActions(),
        'value' => $content ? $content->action : null
        ])
    </div>
    <div class="col-1 form-text">when</div>
    <div class="col-2">
        @include('smorken/dynform::_preset.input._select', [
        'name' => 'logic[element]',
        'items' => $logic->getElements(),
        'value' => $content ? $content->element : null
        ])
    </div>
    <div class="col-2">
        @include('smorken/dynform::_preset.input._select', [
        'name' => 'logic[part]',
        'items' => $logic->getParts(),
        'value' => $content ? $content->part : null
        ])
    </div>
    <div class="col-2">
        @include('smorken/dynform::_preset.input._select', [
        'name' => 'logic[comparator]',
        'items' => $logic->getComparators(),
        'value' => $content ? $content->comparator : null
        ])
    </div>
    <div class="col-2">
        @include('smorken/dynform::_preset.input._input', [
        'name' => 'logic[value]',
        'value' => $content ? $content->value : null,
        'placeholder' => 'value'
        ])
    </div>
    <div class="col-1">
        <button type="button" class="close">
            <span aria-hidden="true" class="text-danger form-text">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
</div>
