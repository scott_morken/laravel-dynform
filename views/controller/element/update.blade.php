@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@include('smorken/dynform::_preset.controller.update', ['title' => 'Element Administration', 'inputs_view' => 'smorken/dynform::controller.element._form'])
