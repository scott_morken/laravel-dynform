<div class="form-group">
    @include('smorken/dynform::_preset.input._label', ['name' => 'name', 'title' => 'Name'])
    @include('smorken/dynform::_preset.input._input', ['name' => 'name'])
    <div class="form-text">
        This is the HTML-friendly name. It will be modified to make the server happy with it.
    </div>
</div>
@include('smorken/dynform::_preset.input.g_input', ['name' => 'label', 'title' => 'Label'])
@include('smorken/dynform::_preset.input.g_select', ['name' => 'builder', 'title' => 'Element', 'items' => $builders ?? []])
@include('smorken/dynform::_preset.input.g_input', ['name' => 'default', 'title' => 'Default Value'])
@include('smorken/dynform::_preset.input.g_textarea', ['name' => 'help', 'title' => 'Help', 'rows' => 3])
@include('smorken/dynform::_preset.input.g_input', ['type' => 'number', 'name' => 'weight', 'title' => 'Weight (position on screen)'])
@php $base = 'smorken/dynform::controller.element.extended.'; @endphp
@include('smorken/dynform::controller.shared.panel_multi',
    [
        'panelcontents' => ($model->options ? $model->options->getOptions(false) : []),
        'type' => 'options',
        'base' => $base,
        'help' => 'Selectors that are plural can be repeated (and require both item and value). Singular selectors can only be used once.',
    ]
)
@include('smorken/dynform::controller.shared.panel_multi',
    [
        'panelcontents' => ($model->validators ? : []),
        'type' => 'validators',
        'base' => $base,
        'help' => 'Validators can be many predefined things (required, date, email, integer). The message will provide a custom error message when validation fails.',
    ]
)
@php $datahelp = \Illuminate\Support\Facades\View::make('smorken/dynform::controller.element.extended._data_help'); @endphp
@include('smorken/dynform::controller.shared.panel_multi',
    [
        'panelcontents' => ($model->data ? $model->data->all(false) : []),
        'type' => 'datum',
        'base' => $base,
        'help' => $datahelp
    ]
)
@include('smorken/dynform::controller.shared.panel_multi',
    [
        'panelcontents' => ($model->logic ? : null),
        'type' => 'logic',
        'base' => $base,
        'help' => 'Form logic. An element can be displayed/hidden based on another element.'
    ]
)
<hr/>
@push('add_to_end')
    @include('smorken/dynform::controller._js._dynform_form')
    <script>
        $(document).ready(function () {
            dynform.shared.init();
        });
    </script>
@endpush
