<script>
    var dynform = dynform || {};
    dynform.reorder = {
        init: function () {
            $.each(dynform.reorder.loaders, function (i, f) {
                f();
            });
        },
        loaders: [
            function () {
                $('#element-list').on('click', '.sort', function (event) {
                    dynform.reorder.reorder();
                    event.preventDefault();
                    return false;
                });
            }
        ],
        reorder: function () {
            $('#order-response').text('');
            var order = {};
            $('.indexed').each(function (index) {
                $(this).data('index', index);
                var id = $(this).data('id');
                var c = $(this).parent().parent().parent();
                var c_id = $(c).data('cid') || 0;
                order[id] = {weight: index, container_id: c_id};
            });
            dynform.reorder.ajax.postOrder(order);
        },
        ajax: {
            postOrder: function (order) {
                var url = '{{ action([config('dynform.forms.controller'), 'postSaveOrderAjax'], ['id' => $model->id]) }}';
                var success = function (data) {
                    $('#order-response').text('Updated!');
                    $(data.element).append(data.content);
                };
                var failure = function (data) {
                    console.log(data);
                    var errordata = {};
                    if (data.status >= 400) {
                        errordata = $.parseJSON(data.responseText);
                    } else {
                        errordata = {message: 'Unknown error [' + data.status + ']'};
                    }
                    console.log(errordata);
                    $('#order-response').text("Error: " + errordata.message);
                };
                $.ajax({
                    url: url,
                    method: 'post',
                    data: {ordered: order, _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: success,
                    error: failure
                });

            }
        }
    };
    $(document).ready(function () {
        dynform.reorder.init();
    });
</script>
