<script>
$(document).ready(function () {
    var elements = $('.sortable-elements');
    for (var i = 0; i < elements.length; i ++) {
        new Sortable(elements[i], {
            group: 'sortable',
            animation: 150,
            fallbackOnBody: true,
            swapThreshold: 0.65
        });
    }
});
</script>
