<script>
    var dynform = dynform || {};
    dynform.shared = {
        cache: {
            options: null,
            validators: null,
            data: null
        },
        init: function () {
            $.each(dynform.shared.loaders, function (i, o) {
                dynform.shared.loaders[i]();
            });
        },
        loaders: [
            function () {
                $('form').on('click', '.new', function (event) {
                    var type = $(this).data('type');
                    var url = dynform.shared.getUrl(type);
                    dynform.shared.ajax.getNewShared(type, url);
                });
            },
            function () {
                $('form').on('click', '.close', function (event) {
                    $(this).parent().parent().remove();
                });
            }
        ],
        getUrl: function (type) {
            var url = null;
            switch (type) {
                case 'options':
                    url = '{{ action([config('dynform.elements.controller'), 'optionAjax']) }}';
                    break;
                case 'validators':
                    url = '{{ action([config('dynform.elements.controller'), 'validatorAjax']) }}';
                    break;
                case 'datum':
                    url = '{{ action([config('dynform.elements.controller'), 'dataAjax']) }}';
                    break;
                case 'logic':
                    url = '{{ action([config('dynform.elements.controller'), 'logicAjax']) }}';
                    break;
                default:
                    break;
            }
            return url;
        },
        ajax: {
            getNewShared: function (type, url) {
                var cdata = dynform.shared.cache[type];
                var success = function (data) {
                    var func = 'append';
                    if (type === 'logic') {
                        func = 'html';
                    }
                    dynform.shared.cache[type] = data;
                    $(data.element)[func](data.content);
                };
                var failure = function (data) {
                    try {
                        var errordata = $.parseJSON(data.responseText);
                    } catch (err) {
                        errordata = {message: data.responseText};
                    }
                    console.log(errordata);
                    alert("Error: " + errordata.message);
                };
                if (cdata) {
                    success(cdata);
                    return;
                }
                if (!url) {
                    failure({"responseText": 'Empty URL.'});
                    return;
                }
                $.get({
                    url: url,
                    dataType: "json",
                    success: success,
                    error: failure
                });
            }
        }
    };
</script>
