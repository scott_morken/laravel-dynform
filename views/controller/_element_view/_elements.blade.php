@php $editing = $editing ?? true; @endphp
@foreach ($elements as $i => $element)
    @if ($element->isContainer())
        @include('smorken/dynform::controller._element_view._container', [
        'element' => $element,
        'index' => $i
        ])
    @else
        @include('smorken/dynform::controller._element_view._element', [
        'element' => $element,
        'index' => $i
        ])
    @endif
@endforeach
