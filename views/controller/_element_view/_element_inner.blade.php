<div class="indexed" data-index="" data-id="{{ $element->id }}" data-weight="{{ $element->weight }}">
    @if ($element->id > 0 && $editing)
        @include('smorken/dynform::controller._element_view._actions')
    @endif
    {{ $element }}
</div>
