<div class="float-right edit-block">
    <?php $controller = config('dynform.forms.controller'); ?>
    <a href="{{ action([$controller, 'deleteElement'], ['id' => $model->id, 'element_id' => $element->id]) }}"
       title="Remove {{ $model->name }}">
        <span class="fas fa-trash"></span>
        <span class="sr-only">Remove element #{{ $element->id }} from form</span>
    </a>
</div>
