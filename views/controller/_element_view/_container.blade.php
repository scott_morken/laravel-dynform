<div class="list-group-item sortable-{{ $element->id }} container" data-cid="{{ $element->id }}">
    @include('smorken/dynform::controller._element_view._element_inner', ['element' => $element])
    <div class="list-group sortable-elements children card p-1">
        @include('smorken/dynform::controller._element_view._elements', ['elements' => $element->getChildren()])
    </div>
</div>

