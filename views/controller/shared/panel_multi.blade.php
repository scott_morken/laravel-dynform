<div class="card mb-2">
    <div class="card-header">
        <button
                type="button"
                id="new-{{ $type }}"
                class="btn btn-info btn-sm float-right new"
                data-type="{{ $type }}">
            <span class="fas fa-plus"></span>
            <span class="sr-only">Add {{ $type }}</span>
        </button>
        <h4 class="card-title">{{ \Illuminate\Support\Str::title($type) }}</h4>
    </div>
    <div class="card-body">
        <div id="{{ $type }}-content">
            @if ($type != 'logic' && $panelcontents)
                @foreach($panelcontents as $panelkey => $panelcontent)
                    @include($base . '_' . \Illuminate\Support\Str::singular($type), array('content' => $panelcontent, 'key' => $panelkey))
                @endforeach
            @elseif ($type == 'logic' && $panelcontents)
                @include($base . '_' . \Illuminate\Support\Str::singular($type), array('content' => $panelcontents))
            @endif
        </div>
    </div>
    @if (isset($help))
        <div class="card-footer">
            <div class="text-muted">{{ $help }}</div>
        </div>
    @endif
</div>
