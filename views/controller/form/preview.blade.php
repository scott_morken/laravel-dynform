@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@section('content')
    @include('smorken/dynform::_preset.controller._to_index')
    @if ($raw === true)
        @php $form->setRawable(true); $form->setRenderable(false); @endphp
        <pre>{{ \Smorken\Support\Arr::stringify($form->create($model, $data)->raw()) }}</pre>
    @else
        {!! $form->create($model, $data)->render() !!}
    @endif
@endsection
