@include('smorken/dynform::_preset.input.g_input', ['name' => 'name', 'title' => 'Name'])
@include('smorken/dynform::controller.shared.panel_multi',
    [
        'panelcontents' => ($model->options ? $model->options->getOptions(false) : null),
        'type' => 'options',
        'base' => 'smorken/dynform::controller.form.extended.',
        'help' => 'Only one option per key will be saved.',
    ]
)
<hr/>
@push('add_to_end')
    @include('smorken/dynform::controller._js._dynform_form')
    <script>
        var dynform = dynform || {shared: {}};
        dynform.shared.getUrl = function (type) {
            var url = null;
            switch (type) {
                case 'options':
                    url = '{{ action([config('dynform.elements.controller'), 'optionAjax']) }}';
                    break;
            }
            return url;
        };
        $(document).ready(function () {
            dynform.shared.init();
        });
    </script>
@endpush
