@php $elements = $model->getElementsNotAttached(); @endphp
<div class="card mt-2 mb-2">
    <div class="card-body">
        @if (count($elements))
            <form method="post" class="form-inline">
                @csrf
                <div class="form-group mb-2 mr-2">
                    @include('smorken/dynform::_preset.input._label', ['name' => 'element_id', 'title' => 'Element', 'label_classes' => 'sr-only'])
                    @include('smorken/dynform::_preset.input._select', [
                    'name' => 'element_id',
                    'items' => ['' => '-- Select One --'] + $elements->keyBy('id')->all()
                    ])
                </div>
                <div class="form-group mb-2 mr-2">
                    @include('smorken/dynform::_preset.input._label', ['name' => 'weight', 'title' => 'Weight', 'label_classes' => 'sr-only'])
                    @include('smorken/dynform::_preset.input._input', [
                    'name' => 'weight',
                    'value' => '',
                    'placeholder' => 'Weight'
                    ])
                </div>
                <div class="form-group mb-2 mr-2">
                    @include('smorken/dynform::_preset.input._label', ['name' => 'container_id', 'title' => 'Container', 'label_classes' => 'sr-only'])
                    @include('smorken/dynform::_preset.input._select', [
                    'name' => 'container_id',
                    'items' => $containers
                    ])
                </div>
                <button type="submit" class="btn btn-primary mb-2 mr-2">Add</button>
            </form>
        @else
            <div class="text-muted">No unused elements available.</div>
        @endif
    </div>
</div>
