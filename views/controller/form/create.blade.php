@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@include('smorken/dynform::_preset.controller.create', ['title' => 'Form Administration', 'inputs_view' => 'smorken/dynform::.controller.form._form'])
