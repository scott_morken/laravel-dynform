@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@section('content')
    @include('smorken/dynform::_preset.controller._title', ['title' => 'Form Administration'])
    @include('smorken/dynform::_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Options</th>
                <th>Elements</th>
                <th>Preview</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>
                        @include('smorken/dynform::_preset.controller.index_actions._view', ['value' => $model->getKey()])
                    </td>
                    <td>{{ $model->name }}</td>
                    <td>
                        @foreach ($model->getOptions()->getOptions(false) as $o)
                            <div>{{ $o }}</div>
                        @endforeach
                    </td>
                    <td>
                        <a href="{{ action([$controller, 'elements'], ['id' => $model->getKey()]) }}"
                           title="Elements for form {{ $model->getKey() }}">
                            elements
                        </a>
                    </td>
                    <td>
                        <a href="{{ action([$controller, 'preview'], ['id' => $model->getKey()]) }}"
                           title="Preview form {{ $model->getKey() }}">
                            preview
                        </a>
                    </td>
                    <td>
                        @include('smorken/dynform::_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
