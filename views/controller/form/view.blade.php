@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@include('smorken/dynform::_preset.controller.view', ['title' => 'Form Administration'])
@section('content')
    <div class="mb-2">
        <h3>Options</h3>
        @foreach ($model->getOptions()->getOptions(false) as $o)
            <div>{{ $o }}</div>
        @endforeach
    </div>
    <div class="mb-2">
        <a href="{{ action([$controller, 'elements'], ['id' => $model->getKey()]) }}"
           title="Elements for form {{ $model->getKey() }}">
            Manage Elements
        </a>
    </div>
    <div class="mb-2">
        <a href="{{ action([$controller, 'preview'], ['id' => $model->getKey()]) }}"
           title="Preview form {{ $model->getKey() }}">
            Preview
        </a>
    </div>
@append
