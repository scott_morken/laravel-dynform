@extends(\Illuminate\Support\Facades\Config::get('dynform.view_master', 'layouts.app'))
@section('content')
    @include('smorken/dynform::_preset.controller._to_index')
    <h4>Elements for {{ $model->name }}</h4>
    <div id="add-element-form">
        @include('smorken/dynform::controller.form._add_element_form')
    </div>
    <hr/>
    <div id="element-list">
        <?php $elements = $model->getStacked(); ?>
        @if (count($elements))
            <noscript class="alert alert-danger">Javascript is required for this to function!</noscript>
            <div id="elements" class="mb-2">
                @include('smorken/dynform::controller._element_view._elements', ['elements' => $elements])
            </div>
            <div>
                <button type="button" class="sort btn btn-primary">Save Order</button>
                <span class="text-info" id="order-response"></span>
            </div>
        @else
            <div class="text-muted">No elements found.</div>
        @endif
    </div>
@endsection
@push('add_to_end')
    @include(config('dynform.js_sortable_include', 'smorken/dynform::controller._js._Sortable'))
    @include(config('dynform.js_sortable_runtime', 'smorken/dynform::controller._js._Sortable_runtime'))
    @include('smorken/dynform::controller._js._reorder')
@endpush
