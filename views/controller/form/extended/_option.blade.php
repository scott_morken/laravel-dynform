<div class="option row form-group">
    <div class="col-3">
        @include('smorken/dynform::_preset.input._select', [
        'name' => 'option[key][]',
        'items' => $options ?? [],
        'value' => $content ? $content->key : null
        ])
        @include('smorken/dynform::_preset.input._input', ['type' => 'hidden', 'name' => 'option[item][]', 'value' => ''])
    </div>
    <div class="col-5">
        @include('smorken/dynform::_preset.input._input', [
        'name' => 'option[value][]',
        'value' => $content ? $content->value : null,
        'placeholder' => 'value'
        ])
    </div>
    <div class="col-1">
        <button type="button" class="close">
            <span aria-hidden="true" class="text-danger form-text">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
</div>
