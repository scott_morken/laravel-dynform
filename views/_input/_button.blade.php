@php
    $type = (!isset($type) || !is_string($type) ? 'submit' : $type);
@endphp
<button type="{{ $type }}" @include('smorken/dynform::_input.__attrs')>
    {{ $element->getLabel() ?? $element->getName() }}
</button>
