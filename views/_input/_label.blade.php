@if ($element->getLabel())
    @if (!isset($attrs['for']))
        @php $attrs['for'] = $element->getId(); @endphp
    @endif
    <label @include('smorken/dynform::_input.__attrs')>
        {{ $element->getLabel() }}
    </label>
@endif
