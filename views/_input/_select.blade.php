<select @include('smorken/dynform::_input.__attrs', $attrs)>
    @php $value = $element->getValue(); @endphp
    @foreach ($items as $k => $v)
        <option value="{{ $k }}" {{ (!is_null($value) && $value == $k) ? 'selected' : '' }}>{{ $v }}</option>
    @endforeach
</select>
