<?php
$attrs = $attrs ?? [];
?>
@foreach($attrs as $attr_key => $attr_value)
    @include('smorken/dynform::_input.__attr')
@endforeach
