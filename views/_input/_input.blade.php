@php
    $type = (!isset($type) || !is_string($type) ? 'text' : $type);
@endphp
<input type="{{ $type }}"
        @include('smorken/dynform::_input.__attrs', $attrs)
        @include('smorken/dynform::_input.__attrs', ['attrs' => $add_attrs ?? []])
>
