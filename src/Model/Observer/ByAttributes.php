<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/7/14
 * Time: 6:05 PM
 */

namespace Smorken\DynForm\Model\Observer;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\App;

class ByAttributes
{

    /**
     * @var Guard
     */
    protected $auth;

    public function saving($model)
    {
        if (!$this->getAuth()->check()) {
            return;
        }
        if (property_exists($model, 'byAttributes') && $model->byAttributes == false) {
            return;
        }
        $user_id = $this->getAuth()->id();
        if (!$model->exists) {
            $model->created_by = $user_id;
        }
        $model->updated_by = $user_id;
    }

    /**
     * @return Guard
     */
    protected function getAuth()
    {
        if (!$this->auth) {
            $this->auth = App::make(Guard::class);
        }
        return $this->auth;
    }
}
