<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/17/14
 * Time: 8:26 AM
 */

namespace Smorken\DynForm\Model\VO;

use Smorken\DynForm\Contracts\Model\Checkable;
use Smorken\DynForm\Contracts\Model\Element;

class ElementPlaceholder implements Element, Checkable
{

    use \Smorken\DynForm\Model\Traits\Element, \Smorken\DynForm\Model\Traits\Checkable;

    protected $builder;

    protected $container;

    protected $data;

    protected $default;

    protected $help;

    protected $id;

    protected $is_checkable;

    protected $is_container;

    protected $label;

    protected $name;

    protected $options;

    protected $validators;

    protected $weight;

    public function copy(Element $element)
    {
        $this->setId($element->getId());
        $this->setName($element->getName());
        $this->setLabel($element->getLabel());
        $this->setBuilder($element->getBuilder());
        $this->setContainer($element->getContainer());
        $this->setDefault($element->getDefault());
        $this->setValue($element->getValue());
        $this->setWeight($element->getWeight());
        $this->setHelp($element->getHelp());
        $this->setOptions($element->getOptions());
        $this->setData($element->getData());
        $this->setValidators($element->getValidators());
        if ($element instanceof Checkable) {
            $this->setIsCheckable($element->isCheckable());
        }
    }

    public function getContainerId()
    {
        if ($this->is_container || !$this->getContainer()) {
            return $this->id;
        }
        if ($this->getContainer()) {
            return $this->getContainer()->getContainerId();
        }
        return null;
    }

    public function getOption($key, $item = null)
    {
        return $this->getOptions()->getOption($key, $item);
    }

    public function getOptions()
    {
        if (!$this->options) {
            $this->options = new Options();
        }
        return $this->options;
    }

    public function setOptions(Options $options)
    {
        $this->options = $options;
    }

    public function isContainer()
    {
        return false;
    }

    public function setOption($key, $item, $value)
    {
        $opts = $this->getOptions();
        $o = new Option($key, $item, $value);
        $opts->addOption($o);
        $this->options = $opts;
    }
}
