<?php


namespace Smorken\DynForm\Model\VO;


use Smorken\Model\VO;

class Element extends VO implements \Smorken\DynForm\Contracts\Model\Element
{

    use \Smorken\DynForm\Model\Traits\Element;

    public function getContainerId()
    {
        return $this->container_id ?? 0;
    }

    public function getOption($key, $item = null)
    {
        return $this->getOptions()->getOption($key, $item);
    }

    public function getOptions()
    {
        if (!$this->options) {
            $this->options = new Options();
        }
        return $this->options;
    }

    public function isContainer()
    {
        return (bool) $this->is_container;
    }

    public function setOption($key, $item, $value)
    {
        $opts = $this->getOptions();
        $o = new Option($key, $item, $value);
        $opts->addOption($o);
        $this->options = $opts;
    }

    public function setOptions(Options $options)
    {
        $this->options = $options;
    }
}
