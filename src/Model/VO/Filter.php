<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/15/14
 * Time: 4:51 PM
 */

namespace Smorken\DynForm\Model\VO;

use Illuminate\Support\Arr;

class Filter extends \Smorken\Support\Filter
{

    public function getFilter($key)
    {
        return Arr::get($this->filters, $key, null);
    }

    public function setFilter($key, $value)
    {
        $this->filters[$key] = $value;
    }
}
