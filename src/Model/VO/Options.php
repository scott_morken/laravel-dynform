<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/15/14
 * Time: 10:45 AM
 */

namespace Smorken\DynForm\Model\VO;

use Illuminate\Support\Arr;

class Options implements \Smorken\DynForm\Contracts\Model\Options
{

    protected $options = [];

    /**
     * @param  array|Option|null  $options
     */
    public function __construct($options = null)
    {
        if (is_array($options)) {
            $this->addOptions($options);
        } else {
            if ($options instanceof \Smorken\DynForm\Contracts\Model\Option) {
                $this->addOption($options);
            }
        }
    }

    public function addOption(\Smorken\DynForm\Contracts\Model\Option $option)
    {
        $k = $option->getKey();
        $this->options[$k] = $option;
    }

    public function addOptions(array $options)
    {
        foreach ($options as $option) {
            $this->addOption($option);
        }
    }

    public function getOption($key, $item = null)
    {
        $k = $key.($item ? '.'.$item : null);
        return Arr::get($this->toArray(), $k, null);
    }

    public function getOptions($toArray = true)
    {
        if ($toArray) {
            return $this->toArray();
        }
        return $this->options;
    }

    public function toArray()
    {
        $arr = [];
        foreach ($this->options as $k => $option) {
            Arr::set($arr, $k, $option->getValue());
        }
        return $arr;
    }
}
