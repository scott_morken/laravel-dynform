<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/13/14
 * Time: 2:00 PM
 */

namespace Smorken\DynForm\Model\VO;

class Validator
{

    public $message;

    public $validators;

    public function __construct($validators, $message = null)
    {
        $this->setValidator($validators, $message);
    }

    public function __toString()
    {
        return $this->validators;
    }

    public function getValidator()
    {
        return $this;
    }

    public function setValidator($validators, $message = null)
    {
        $this->validators = $validators;
        $this->message = $message;
    }
}
