<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/12/14
 * Time: 11:22 AM
 */

namespace Smorken\DynForm\Model\VO;

class Logic implements \Smorken\DynForm\Contracts\Model\Logic
{

    public $action = 'show';

    public $comparator = '=';

    public $element;

    public $part = 'value';

    public $value;
}
