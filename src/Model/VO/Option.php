<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/13/14
 * Time: 2:00 PM
 */

namespace Smorken\DynForm\Model\VO;

class Option implements \Smorken\DynForm\Contracts\Model\Option
{

    public $item;

    public $key;

    public $value;

    public function __construct($key, $item, $value)
    {
        $this->key = $key;
        $this->item = $item;
        $this->value = $value;
    }

    public function __toString()
    {
        $str = [$this->key];
        if ($this->item) {
            $str[] = $this->item;
        }
        $str[] = $this->value;
        return implode(' ', $str);
    }

    public function getKey()
    {
        return $this->key.($this->item ? '.'.$this->item : null);
    }

    public function getValue()
    {
        return $this->value;
    }
}
