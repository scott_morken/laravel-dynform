<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/13/14
 * Time: 2:00 PM
 */

namespace Smorken\DynForm\Model\VO;

use Smorken\DynForm\LookupHandler;

class Data implements \Smorken\DynForm\Contracts\Model\Data
{

    protected $data = [];

    protected $keys;

    protected $position = 0;

    public function __construct(array $data = [])
    {
        $this->setData($data);
    }

    public function addData($key, $value)
    {
        if (!in_array($key, $this->keys)) {
            $this->keys[] = $key;
        }
        $this->data[$key] = $value;
    }

    public function all($expand = true)
    {
        if ($expand && in_array('php', $this->keys)) {
            $this->expandDataFromLookup();
        }
        return $this->data;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->data[$this->key()];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->keys[$this->position];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        if (in_array('php', $this->keys)) {
            $this->expandDataFromLookup();
        }
        $this->position = 0;
    }

    public function setData(array $data)
    {
        $this->position = 0;
        $this->keys = array_keys($data);
        $this->data = $data;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->keys[$this->position]);
    }

    protected function expandDataFromLookup()
    {
        $data = [];
        $keys = [];
        $lh = new LookupHandler();
        foreach ($this->keys as $index => $key) {
            if ($key == 'php') {
                $results = $lh->run($this->data[$key]);
                foreach ($results as $k => $v) {
                    $data[$k] = $v;
                    $keys[] = $k;
                }
            } else {
                $data[$key] = $this->data[$key];
                $keys[] = $key;
            }
        }
        $this->data = $data;
        $this->keys = $keys;
    }
}
