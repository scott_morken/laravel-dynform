<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 8:30 AM
 */

namespace Smorken\DynForm\Model\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Smorken\DynForm\Contracts\Model\Element as ElementModel;
use Smorken\DynForm\Contracts\Model\Form;
use Smorken\DynForm\Contracts\Storage\Builder;
use Smorken\DynForm\Element\ElementException;
use Smorken\DynForm\Model\VO\Data;
use Smorken\DynForm\Model\VO\Logic;

trait Element
{

    protected $_container;

    protected $_labelAttributes = [];

    protected $children;

    protected $errors;

    protected $id_forced = false;

    protected $multi = [];

    public function __toString()
    {
        $parts = [];
        $label = $this->getLabel();
        $name = $this->getName();
        $builder = $this->getBuilder();
        $weight = $this->getWeight();
        if ($label) {
            $parts[] = $label.':';
        }
        $parts[] = '#'.$this->id;
        $parts[] = $name;
        $parts[] = '('.$builder.')';
        $parts[] = '['.$weight.']';
        $parts[] = $this->isContainer() ? '(container)' : '';
        return implode(' ', $parts);
    }

    public function addChild(\Smorken\DynForm\Contracts\Model\Element $element)
    {
        $this->getChildren()->push($element);
    }

    public function addMultiElement(ElementModel $element)
    {
        $id = $element->getId();
        $this->multi[$id] = $element;
    }

    public function getAllElementAttributes($additional = [])
    {
        $attributes = $this->getElementAttributes($additional);
        $attributes['name'] = $this->getName();
        $attributes['id'] = $this->getId();
        if ($this->getValue()) {
            $attributes['value'] = $this->getValue();
        }
        if ($this->isChecked()) {
            $attributes['checked'] = $this->isChecked();
        }
        return $attributes;
    }

    /**
     * @return mixed
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    public function getChildren()
    {
        if (!$this->children) {
            $this->children = new Collection();
        }
        return $this->children;
    }

    public function setChildren(Collection $children)
    {
        $this->children = $children;
    }

    public function getChildrenAttribute()
    {
        return $this->getChildren();
    }

    public function getContainer()
    {
        return $this->_container ?: ($this->container_id ? $this->container->container : null);
    }

    public function setContainer($container)
    {
        $this->_container = $container;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function getElementAttr($key)
    {
        return $this->getOption('attributes', $key);
    }

    public function getElementAttributes($additional = [])
    {
        $attributes = $this->getOption('attributes') ?: [];
        $appends = ['class'];
        return $this->buildAttributeArray($attributes, $appends, $additional);
    }

    public function getElementAttributesAsString($additional = [])
    {
        $attrs = $this->getElementAttributes($additional);
        return implode(' ', $this->mapAttributes($attrs));
    }

    public function getElementAttributesExcept(array $except, $additional = [])
    {
        $attributes = $this->getAllElementAttributes($additional);
        foreach ($except as $k) {
            unset($attributes[$k]);
        }
        return $attributes;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function getHelp()
    {
        return $this->help;
    }

    public function getId($append = null)
    {
        if ($this->id_forced || $this->getElementAttr('id')) {
            return $this->getElementAttr('id');
        }
        return $this->getStrippedName($append);
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getLabelAttr($key)
    {
        return $this->getOption('label_attributes', $key);
    }

    public function getLabelAttributes($additional = [])
    {
        $attributes = $this->getOption('label_attributes') ?: [];
        $appends = ['class'];
        return $this->buildAttributeArray($attributes, $appends, $additional);
    }

    public function setLabelAttributes($label_opts)
    {
        foreach ($label_opts as $k => $v) {
            $this->setLabelAttr($k, $v);
        }
    }

    public function getLabelAttributesAsString($additional = [])
    {
        $attrs = $this->getLabelAttributes($additional);
        return implode(' ', $this->mapAttributes($attrs));
    }

    public function getLogic()
    {
        return $this->logic;
    }

    public function getModelId()
    {
        return $this->id;
    }

    public function getMultiElement($id)
    {
        return Arr::get($this->multi, $id, null);
    }

    public function getMultiElements()
    {
        return $this->multi;
    }

    public function getName()
    {
        if (!$this->name) {
            $this->name = sprintf('element-'.rand(1, 1000));
        }
        return $this->name;
    }

    /**
     * NOT IN USE
     *
     * @param $key
     * @return bool|mixed
     */
    public function getOptionItems($key)
    {
        if ($key === null) {
            return true;
        }
        $items = [
            'attributes' => ['class', 'id', 'placeholder', 'maxlength'],
        ];
        return Arr::get($items, $key, false);
    }

    public function getOptionKeys()
    {
        return [
            'view' => 'View',
            'attributes' => 'Element Attributes',
            'label_attributes' => 'Label Attributes',
            'wrapper' => 'Wrapper',
            'wrapper_attributes' => 'Wrapper Attributes',
        ];
    }

    /**
     * NOT IN USE
     *
     * @param      $key
     * @param  null  $item
     * @return bool|mixed
     */
    public function getOptionValues($key, $item = null)
    {
        if ($key === null && $item === null) {
            return true;
        }
        $pkey = $key.($item ? '.'.$item : null);
        $values = [
            'method' => ['GET' => 'GET', 'POST' => 'POST'],
        ];
        return Arr::get($values, $pkey, false);
    }

    public function getValidators()
    {
        return $this->validators;
    }

    public function getValue()
    {
        if ($this->value === null) {
            return $this->getDefault();
        }
        return $this->value;
    }

    public function getWeight()
    {
        return ($this->pivot !== null ? $this->pivot->weight : $this->weight);
    }

    public function getWrapper()
    {
        return $this->getOption('wrapper', 'obj') ?: null;
    }

    public function getWrapperAttr($key)
    {
        return $this->getOption('wrapper_attributes', $key);
    }

    public function getWrapperAttributes($additional = [])
    {
        $attributes = $this->getOption('wrapper_attributes') ?: [];
        $appends = ['class'];
        return $this->buildAttributeArray($attributes, $appends, $additional);
    }

    public function getWrapperAttributesAsString($additional = [])
    {
        $attrs = $this->getWrapperAttributes($additional);
        return implode(' ', $this->mapAttributes($attrs));
    }

    public function hasErrors()
    {
        return $this->errors && count($this->errors);
    }

    public function hasLogic()
    {
        return ($this->logic && $this->logic instanceof Logic);
    }

    public function hasWrapper()
    {
        $wrapper = $this->getWrapper();
        $bp = App::make(Builder::class);
        $wrappers = $bp->allWrappers();
        $allowed = array_keys($wrappers);
        if ($wrapper && in_array($wrapper, $allowed)) {
            return true;
        }
        return false;
    }

    public function isCheckable()
    {
        return (bool) $this->getOptions()
                           ->getOption('is_checkable');
    }

    public function setBuilder($builder)
    {
        $this->builder = $builder;
    }

    public function setData($data)
    {
        if (is_array($data)) {
            $data = new Data($data);
        }
        $this->data = $data;
    }

    public function setDefault($default)
    {
        $this->default = $default;
    }

    public function setElementAttr($key, $value)
    {
        $this->setOption('attributes', $key, $value);
    }

    public function setElementAttributes($options)
    {
        foreach ($options as $k => $v) {
            $this->setElementAttr($k, $v);
        }
    }

    public function setForm(Form $form)
    {
        throw new ElementException("Set form cannot be called on this model.");
    }

    public function setHelp($help)
    {
        $this->help = $help;
    }

    public function setId($id)
    {
        $this->id_forced = true;
        $this->setElementAttr('id', $id);
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setLabelAttr($key, $value)
    {
        $this->setOption('label_attributes', $key, $value);
    }

    public function setLogic(Logic $logic)
    {
        $this->logic = $logic;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setValidators($validators)
    {
        $this->validators = $validators;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function setWrapper($wrapper)
    {
        $this->setOption('wrapper', 'obj', $wrapper);
    }

    public function setWrapperAttr($key, $value)
    {
        $this->setOption('wrapper_attributes', $key, $value);
    }

    public function setWrapperAttributes($options)
    {
        foreach ($options as $k => $v) {
            $this->setWrapperAttr($k, $v);
        }
    }

    public function validate($value = null, $data = [])
    {
        $validators = $this->getValidators();
        if (!$validators) {
            return true;
        }
        $messages = [];
        $valarray = [];
        foreach ($validators as $v) {
            $valarray[] = $v->validators;
            if ($v->message) {
                $messages[$v->validators] = $v->message;
            }
        }
        $data[$this->getName()] = $value;
        $val = Validator::make($data, [$this->getName() => $valarray], $messages);
        $passes = $val->passes();
        if (!$passes) {
            $this->errors = $val->messages();
        }
        return $passes;
    }

    protected function buildAttributeArray(array $attributes, array $appends, array $additional)
    {
        foreach ($additional as $k => $v) {
            if (in_array($k, $appends)) {
                $currval = Arr::get($attributes, $k, null);
                $attributes[$k] = $currval ? $currval.' '.$v : $v;
            } else {
                $attributes[$k] = $v;
            }
        }
        return $attributes;
    }

    protected function getStrippedName($append = null)
    {
        $sn = [$this->strip($this->getName())];
        if ($append) {
            $sn[] = $this->strip($append);
        } elseif (!$this->multi && $this->isCheckable() && $this->getValue()) {
            $sn[] = $this->strip($this->getValue());
        }
        return implode('-', $sn);
    }

    protected function mapAttributes($attrs)
    {
        return array_map(
            function ($k) use ($attrs) {
                return $k.'="'.$attrs[$k].'"';
            },
            array_keys($attrs)
        );
    }

    protected function strip($str)
    {
        return preg_replace('/[-]{2,}/', '-', Str::slug(str_replace(['[', ']'], '-', $str)));
    }
}
