<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 8:30 AM
 */

namespace Smorken\DynForm\Model\Traits;

trait Checkable
{

    /**
     * @return false|mixed
     */
    public function hiddenValue()
    {
        $v = $this->getOptions()
                  ->getOption('hidden_value');
        if (strlen($v)) {
            return $v;
        }
        return false;
    }

    /**
     * @param  bool  $default
     * @return boolean
     */
    public function isChecked($default = true)
    {
        $is = $this->getOptions()
                   ->getOption('is_checked');
        if (is_null($is) && $default) {
            $is = !is_null($this->getDefault()) && ($this->getValue() === $this->getDefault());
        }
        return (bool) $is;
    }

    public function setChecked($checked = false)
    {
        $this->setOption('is_checked', null, $checked);
    }

    public function setHiddenValue($value = false)
    {
        $this->setOption('hidden_value', null, $value);
    }

    public function setIsCheckable($checkable = false)
    {
        $this->setOption('is_checkable', null, $checkable);
    }
}
