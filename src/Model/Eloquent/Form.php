<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 9:08 AM
 */

namespace Smorken\DynForm\Model\Eloquent;

use DateTime;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Class Form
 * @package Smorken\DynForm\Model\Eloquent
 *
 * @property integer $id
 * @property string $name
 * @property array $options
 * @property DateTime $created_at
 * @property DateTime $updated_at
 *
 * Relations
 * @property Element[] $elements
 * @property Entry[] $entries
 */
class Form extends Base implements \Smorken\DynForm\Contracts\Model\Form
{

    protected $_containers;

    protected $_elements;

    protected $_id;

    protected $fillable = [
        'name',
        'options',
    ];

    protected $parsed;

    protected $rules = [
        'name' => 'required|min:1|max:64',
    ];

    public function __toString()
    {
        return $this->getName().' (form)';
    }

    public function elements()
    {
        return $this->belongsToMany(
            $this->getFromConfig('dynform.elements.model'),
            $this->getTableFromName('element_form'),
            'form_id',
            'element_id'
        )
                    ->withPivot('weight', 'container_id')
                    ->orderBy('pivot_weight');
    }

    public function entries()
    {
        return $this->hasMany($this->getFromConfig('dynform.entries.model'), 'form_id');
    }

    public function getElements()
    {
        if (!$this->_elements || count($this->_elements) == 0) {
            $this->_elements = new \Illuminate\Support\Collection();
            $es = $this->elements;
            foreach ($es as $e) {
                $this->_elements->put($e->id, $e);
            }
        }
        return $this->_elements;
    }

    public function getElementsNotAttached()
    {
        $elementProvider = $this->getApp()->make(\Smorken\DynForm\Contracts\Handler\Storage::class)->elementProvider();
        $elementModel = $elementProvider->getModel();
        $currElements = $this->getElements();
        $query = $elementModel->newQuery()
                              ->orderBy('is_container')
                              ->orderBy('builder')
                              ->orderBy('name');
        if (count($currElements)) {
            $ids = $currElements->pluck('id')->toArray();
            $elements = $query
                ->whereNotIn('id', $ids)
                ->get();
        } else {
            $elements = $query->get();
        }
        return $elements;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getModelId()
    {
        return $this->id;
    }

    public function getName()
    {
        if (!$this->name) {
            $this->name = sprintf('Form '.rand(1, 1000));
        }
        return $this->name;
    }

    public function getOptionItems($key)
    {
        if ($key === null) {
            return true;
        }
        $items = [];
        return Arr::get($items, $key, false);
    }

    public function getOptionKeys()
    {
        return [
            'method' => 'Method',
            'url' => 'Action/URL',
            'class' => 'Classes',
        ];
    }

    public function getOptionValues($key, $item = null)
    {
        if ($key === null && $item === null) {
            return true;
        }
        $pkey = $key.($item ? '.'.$item : null);
        $values = [
            'method' => ['GET' => 'GET', 'POST' => 'POST'],
        ];
        return Arr::get($values, $pkey, false);
    }

    public function getStacked()
    {
        if (!$this->parsed) {
            $c = $this->createContainerList($this->elements);
            $this->parsed = $this->stripElements($c);
        }
        return $this->parsed;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    protected function addChildrenToContainers(Collection $elements, Collection $containers)
    {
        foreach ($elements as $element) {
            $cid = $element->getContainerId();
            $container = $containers->get($cid);
            if ($container !== $element) {
                $container->addChild($element);
            }
        }
        return $containers;
    }

    protected function stripElements(Collection $elements)
    {
        foreach ($elements as $id => $element) {
            if ($element->id !== 0) {
                $elements->forget($id);
            }
        }
        return $elements;
    }

    protected function createContainerList(Collection $elements)
    {
        $containers = new Collection();
        $zero = new \Smorken\DynForm\Model\VO\Element(['id' => 0, 'is_container' => true, 'name' => 'default-wrapper']);
        $containers->put(0, $zero);
        foreach ($elements as $element) {
            if ($element->isContainer() && !$containers->has($element->id)) {
                $containers->put($element->id, $element);
            }
        }
        return $this->addChildrenToContainers($elements, $containers);
    }

    protected function hasContainer($id)
    {
        return $this->getContainers()->has('id');
    }
}
