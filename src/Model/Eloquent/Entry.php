<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/29/14
 * Time: 9:53 AM
 */

namespace Smorken\DynForm\Model\Eloquent;

/**
 * Class Entry
 * @package Smorken\DynForm\Model\Eloquent
 *
 * @property integer $id
 * @property integer $form_id
 * @property integer $created_by
 * @property integer $updated_by
 *
 * Relations
 * @property \Smorken\DynForm\Contracts\Model\Form $form
 * @property \Smorken\DynForm\Contracts\Model\EntryData[] $data
 */
class Entry extends Base implements \Smorken\DynForm\Contracts\Model\Entry
{

    public $byAttributes = true;

    protected $_elements;

    protected $fillable = [
        'form_id',
        'created_by',
        'updated_by',
    ];

    protected $rules = [
        'form_id' => 'required|integer|min:1',
    ];

    public function __toString()
    {
        return sprintf("%s: Form %s by %s", $this->id, $this->form_id, $this->created_by);
    }

    public function data()
    {
        return $this->hasMany($this->getFromConfig('dynform.entry_data.model'), 'entry_id')
                    ->select(
                        $this->getTableFromName('element_form').'.*',
                        $this->getTableFromName('entry_data').'.*'
                    )
                    ->join(
                        $this->getTableFromName('entries'),
                        $this->getTableFromName('entries').'.id',
                        '=',
                        $this->getTableFromName('entry_data').'.entry_id'
                    )
                    ->join(
                        $this->getTableFromName('element_form'),
                        function ($join) {
                            $join->on(
                                $this->getTableFromName('element_form').'.element_id',
                                '=',
                                $this->getTableFromName('entry_data').'.element_id'
                            )
                                 ->on(
                                     $this->getTableFromName('element_form').'.form_id',
                                     '=',
                                     $this->getTableFromName('entries').'.form_id'
                                 );
                        }
                    )
                    ->orderBy($this->getTableFromName('element_form').'.weight');
    }

    public function dataKeyed($convert = false)
    {
        $data = $this->data;
        $keyed = [];
        foreach ($data as $d) {
            foreach ($this->elements as $name => $element) {
                if ($element->id == $d->element_id) {
                    if (!$convert) {
                        $keyed[$name] = $d->value;
                    } else {
                        $keyed[$name] = $d->valueConvert();
                    }
                }
            }
        }
        return $keyed;
    }

    public function form()
    {
        return $this->belongsTo($this->getFromConfig('dynform.forms.model'), 'form_id');
    }

    public function getElementsAttribute()
    {
        if (!$this->_elements) {
            $elements = $this->form->elements;
            foreach ($elements as $element) {
                $name = $element->name;
                $name = str_replace(['[', ']'], ['.', ''], $name);
                $name = trim($name, '.');
                $this->_elements[$name] = $element;
            }
        }
        return $this->_elements;
    }
}
