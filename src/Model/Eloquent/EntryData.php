<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/29/14
 * Time: 10:03 AM
 */

namespace Smorken\DynForm\Model\Eloquent;

/**
 * Class EntryData
 * @package Smorken\DynForm\Model\Eloquent
 *
 * @property integer $id
 * @property integer $entry_id
 * @property integer $element_id
 * @property string $value
 */
class EntryData extends Base implements \Smorken\DynForm\Contracts\Model\EntryData
{

    protected $fillable = [
        'entry_id',
        'element_id',
        'value',
    ];

    protected $rules = [
        'entry_id' => 'required|integer|min:1',
        'element_id' => 'required|integer|min:1',
    ];

    protected $table = 'entry_data';

    public function __toString()
    {
        return sprintf("%s: Entry: %s Element: %s", $this->id, $this->entry_id, $this->element_id);
    }

    public function element()
    {
        return $this->belongsTo($this->getFromConfig('dynform.elements.model'), 'element_id');
    }

    public function entry()
    {
        return $this->belongsTo($this->getFromConfig('dynform.entries.model'), 'entry_id');
    }

    public function getValueAttribute($value)
    {
        return unserialize($value);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = serialize($value);
    }

    public function valueConvert()
    {
        $eledata = $this->element->data->all();
        $data = $this->value;
        if (is_object($data)) {
            $data = (array) $data;
        }
        if (is_array($data)) {
            $data = $this->modifyDataArrayByElementData($data, $eledata);
        } else {
            $data = $this->modifyDataByElementData($data, $eledata);
        }
        return $data;
    }

    protected function modifyDataArrayByElementData($data, $elementdata)
    {
        $ndata = $data;
        foreach ($data as $k => $v) {
            $ndata[$k] = $this->modifyDataByElementData($v, $elementdata);
        }
        return $ndata;
    }

    protected function modifyDataByElementData($value, $elementdata)
    {
        if (!is_array($value) && $elementdata && isset($elementdata[$value])) {
            return $elementdata[$value];
        }
        return $value;
    }
}
