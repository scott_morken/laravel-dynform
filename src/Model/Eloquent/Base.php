<?php

namespace Smorken\DynForm\Model\Eloquent;

use Illuminate\Container\Container;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Str;
use Smorken\DynForm\Model\VO\Option;
use Smorken\DynForm\Model\VO\Options;
use Smorken\Model\Eloquent;

/**
 * Class AbstractModel
 * @package Smorken\DynForm\Model\Eloquent
 */
abstract class Base extends Eloquent
{

    /**
     * @var \Illuminate\Contracts\Container\Container
     */
    protected $app;

    /**
     * @var Repository
     */
    protected $config;

    protected $prefix;

    public function getNameAttribute()
    {
        return $this->attributes['name'] ?? null;
    }

    public function getOption($key, $item = null)
    {
        return $this->options->getOption($key, $item);
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getOptionsAttribute($value)
    {
        return $this->unserialOrOptions($value);
    }

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable()
    {
        $prefix = $this->getPrefix();
        if (isset($this->table)) {
            return $this->getTableFromName($this->table);
        }

        return ($prefix ? $prefix.'_' : '').str_replace('\\', '', Str::snake(Str::plural(class_basename($this))));
    }

    public function getTableFromName($table)
    {
        $prefix = $this->getPrefix();
        return ($prefix ? $prefix.'_' : '').$table;
    }

    public function name()
    {
        return (string) $this;
    }

    public function setOption($key, $item, $value)
    {
        $opts = $this->options;
        $o = new Option($key, $item, $value);
        $opts->addOption($o);
        $this->options = $opts;
    }

    public function setOptions(Options $options)
    {
        $this->options = $options;
    }

    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = serialize($value);
    }

    protected function getApp()
    {
        if (!$this->app) {
            $this->app = Container::getInstance();
        }
        return $this->app;
    }

    public function setApp(\Illuminate\Contracts\Container\Container $app)
    {
        $this->app = $app;
    }

    protected function getFromConfig($key, $default = null)
    {
        if (!$this->config) {
            $this->config = $this->getApp()->make(Repository::class);
        }
        return $this->config->get($key, $default);
    }

    protected function getPrefix()
    {
        if (is_null($this->prefix)) {
            $this->prefix = $this->getFromConfig('dynform.prefix', false);
        }
        return $this->prefix;
    }

    protected function unserialOrArray($value)
    {
        $v = unserialize($value);
        if (is_array($v)) {
            return $v;
        }
        return [];
    }

    protected function unserialOrOptions($value)
    {
        $v = unserialize($value);
        if ($v instanceof Options) {
            return $v;
        }
        return new Options();
    }
}
