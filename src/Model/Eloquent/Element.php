<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 9:09 AM
 */

namespace Smorken\DynForm\Model\Eloquent;

use Illuminate\Support\Arr;
use Smorken\DynForm\Contracts\Model\Checkable;
use Smorken\DynForm\Contracts\Storage\Builder;
use Smorken\DynForm\Model\VO\Data;
use Smorken\DynForm\Model\VO\Logic;
use Smorken\DynForm\Model\VO\Options;

/**
 * Class FormElement
 *
 * @package Smorken\DynForm\Model\Eloquent
 *
 * @property integer $id
 * @property boolean $is_container
 * @property string $builder
 * @property string $name
 * @property string|null $label
 * @property string|null $default
 * @property string|null $help
 * @property array|null $validators
 * @property Options|null $options
 * @property array|null $data
 * @property array|null $logic
 * @property integer $weight
 *
 * Relations
 * @property Form $form
 * @property Element|null $formContainer
 */
class Element extends Base implements \Smorken\DynForm\Contracts\Model\Element, Checkable
{

    use \Smorken\DynForm\Model\Traits\Element, \Smorken\DynForm\Model\Traits\Checkable;

    protected $fillable = [
        'is_container',
        'builder',
        'name',
        'label',
        'default',
        'help',
        'validators',
        'options',
        'data',
        'logic',
        'weight',
    ];

    protected $rules = [
        'name' => 'required|min:1|max:64',
        'builder' => 'required|min:1|max:128',
        'weight' => 'required|integer|min:0|max:99',
        'help' => 'max:255',
        'default' => 'max:64',
        'label' => 'max:128',
    ];

    public function formContainer()
    {
        return $this->belongsTo($this->getFromConfig('dynform.elements.model'), 'form_container_id');
    }

    public function forms()
    {
        return $this->belongsToMany(
            $this->getFromConfig('dynform.forms.model'),
            $this->getTableFromName('element_form'),
            'element_id',
            'form_id'
        );
    }

    public function getContainerId()
    {
        return $this->pivot && $this->pivot->container_id ? $this->pivot->container_id : ($this->container_id ?? 0);
    }

    public function getDataAttribute($value)
    {
        return $this->unserialOrData($value);
    }

    public function getLogicAttribute($value)
    {
        return $this->unserialLogicOrNull($value);
    }

    public function getValidatorsAttribute($value)
    {
        return $this->unserialOrArray($value);
    }

    public function getWeightAttribute($v)
    {
        if ($this->pivot && $this->pivot->weight !== null) {
            return $this->pivot->weight;
        }
        return Arr::get($this->attributes, 'weight', 0);
    }

    public function isContainer()
    {
        $bid = $this->getBuilder();
        $bp = $this->getApp()->make(Builder::class);
        $builder = $bp->find($bid);
        if ($builder) {
            $this->attributes['is_container'] = $builder->isContainer();
        }
        return $this->attributes['is_container'] ?? false;
    }

    public function scopeByBuilder($query, $builder)
    {
        if (strlen($builder)) {
            $query->where('builder', '=', $builder);
        }
        return $query;
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('is_container')
                     ->orderBy('builder')
                     ->orderBy('name');
    }

    public function scopeIsContainer($query, $is_container)
    {
        $allowed = ['0', '1'];
        if (in_array($is_container, $allowed)) {
            $query->where('is_container', '=', $is_container);
        }
        return $query;
    }

    public function scopeLikeLabel($query, $label)
    {
        if ($label) {
            $query->where('label', 'LIKE', "%$label%");
        }
        return $query;
    }

    public function scopeLikeName($query, $name)
    {
        if ($name) {
            $query->where('name', 'LIKE', "%$name%");
        }
        return $query;
    }

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    public function setIsContainerAttribute($ic)
    {
        $this->attributes['is_container'] = $this->isContainer() ?: $ic;
    }

    public function setLogicAttribute($value)
    {
        $this->attributes['logic'] = serialize($value);
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = preg_replace('/[^\w\[\]-]/', '', $name);
    }

    public function setValidatorsAttribute($value)
    {
        $this->attributes['validators'] = serialize($value);
    }

    public function toArray()
    {
        $arr = parent::toArray();
        $arr['children'] = $this->getChildren()->toArray();
        return $arr;
    }

    protected function unserialLogicOrNull($value)
    {
        $v = unserialize($value);
        if ($v instanceof Logic) {
            return $v;
        }
        return null;
    }

    protected function unserialOrData($value)
    {
        $v = unserialize($value);
        if ($v instanceof Data) {
            return $v;
        }
        return new Data([]);
    }
}
