<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 2:52 PM
 */

namespace Smorken\DynForm\Storage\Eloquent;

class Form extends Base implements \Smorken\DynForm\Contracts\Storage\Form
{

    public function addElement($model, $element, $attributes)
    {
        if (!$attributes['weight']) {
            $attributes['weight'] = $element->weight;
        }
        return $model->elements()->save($element, $attributes);
    }

    public function detachAllElements($model)
    {
        $model->elements()->detach();
    }

    public function detachElement($model, $element_id)
    {
        return $model->elements()->detach($element_id);
    }

    public function getContainerSelectList($model)
    {
        $elements = $model
            ->elements()
            ->where('is_container', '=', 1)
            ->orderBy('builder')
            ->orderBy('name')
            ->get();
        $list = ['0' => 'No Container'];
        foreach ($elements as $m) {
            $list[$m->id] = (string) $m;
        }
        return $list;
    }

    public function getOptionItems($key)
    {
        return $this->getModel()->getOptionItems($key);
    }

    public function getOptionKeys()
    {
        return $this->getModel()->getOptionKeys();
    }

    public function getOptionValues($key, $item = null)
    {
        return $this->getModel()->getOptionValues($key, $item);
    }

    public function updateOrder($model, $order = [])
    {
        return $model->elements()->sync($order);
    }
}
