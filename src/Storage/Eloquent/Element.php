<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 2:52 PM
 */

namespace Smorken\DynForm\Storage\Eloquent;

class Element extends Base implements \Smorken\DynForm\Contracts\Storage\Element
{

    public function getOptionItems($key)
    {
        return $this->getModel()->getOptionItems($key);
    }

    public function getOptionKeys()
    {
        return $this->getModel()->getOptionKeys();
    }

    public function getOptionValues($key, $item = null)
    {
        return $this->getModel()->getOptionValues($key, $item);
    }

    protected function filterByBuilder($q, $v)
    {
        return $q->byBuilder($v);
    }

    protected function filterIsContainer($q, $v)
    {
        return $q->isContainer($v);
    }

    protected function filterLikeLabel($q, $v)
    {
        return $q->likeLabel($v);
    }

    protected function filterLikeName($q, $v)
    {
        return $q->likeName($v);
    }

    protected function getFilterMethods(): array
    {
        return [
            'builder' => 'filterByBuilder',
            'is_container' => 'filterIsContainer',
            'name' => 'filterLikeName',
            'label' => 'filterLikeLabel',
        ];
    }
}
