<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/29/14
 * Time: 10:12 AM
 */

namespace Smorken\DynForm\Storage\Eloquent;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Support\Facades\App;

class EntryData extends Base implements \Smorken\DynForm\Contracts\Storage\EntryData
{

    /**
     * @var MessageBag
     */
    protected $errors;

    public function errors()
    {
        if ($this->hasErrors()) {
            return $this->errors;
        }
        return false;
    }

    public function saveData($form, $entry_id, $data = [])
    {
        $handler = App::make(\Smorken\DynForm\Contracts\Handler\Form::class);
        $v = $handler->validate($form, $data);
        if ($v) {
            $this->getModel()->where('entry_id', $entry_id)->delete();
            $elements = $form->elements;
            foreach ($elements as $element) {
                if (isset($data[$element->name])) {
                    $attrs = [
                        'entry_id' => $entry_id,
                        'element_id' => $element->id,
                        'value' => $data[$element->name],
                    ];
                    $datamodel = $this->getModel()->newInstance($attrs);
                    if (!$datamodel->save()) {
                        $this->addError(class_basename($datamodel), 'Save error.');
                    }
                }
            }
            $form->touch();
        } else {
            $this->errors = $handler->getErrors();
        }
        return !$this->hasErrors();
    }

    protected function addError($key, $msg)
    {
        if (!$this->errors) {
            $this->errors = new \Illuminate\Support\MessageBag();
        }
        $this->errors->add($key, $msg);
    }

    protected function hasErrors()
    {
        if (is_null($this->errors)) {
            return false;
        }
        return $this->errors->count() > 0;
    }
}
