<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/29/14
 * Time: 10:12 AM
 */

namespace Smorken\DynForm\Storage\Eloquent;

class Entry extends Base implements \Smorken\DynForm\Contracts\Storage\Entry
{

    public function delete($model): bool
    {
        if (!is_object($model)) {
            $model = $this->model->find($model);
        }
        $model->data()->delete();
        return $model->delete();
    }

    public function getDataKeyed($model)
    {
        return $model->dataKeyed();
    }
}
