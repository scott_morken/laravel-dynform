<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/10/14
 * Time: 9:47 AM
 */

namespace Smorken\DynForm\Storage\Config;

use Illuminate\Support\Arr;

class Lookup implements \Smorken\DynForm\Contracts\Storage\Lookup
{

    protected $lookups = [];

    public function __construct($lookups)
    {
        $this->lookups = $lookups;
    }

    public function all()
    {
        return $this->lookups;
    }

    public function allByType($type)
    {
        $lookups = $this->all();
        return Arr::get($lookups, $type, null);
    }
}
