<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/12/14
 * Time: 11:25 AM
 */

namespace Smorken\DynForm\Storage\Config;

use Illuminate\Support\Arr;
use Smorken\DynForm\Contracts\Handler\Storage;

class Logic implements \Smorken\DynForm\Contracts\Storage\Logic
{

    /**
     * @var
     */
    protected $config;

    /**
     * @var Storage
     */
    protected $storage;

    public function __construct($config, Storage $storage)
    {
        $this->config = $config;
        $this->storage = $storage;
    }

    public function getActions()
    {
        return Arr::get($this->config, 'actions', []);
    }

    public function getBackend()
    {
        return Arr::get($this->config, 'backend', null);
    }

    public function getComparators()
    {
        return Arr::get($this->config, 'comparators', []);
    }

    public function getElements($form = null)
    {
        $elements = [];
        if ($form) {
            $telements = $form->elements;
        } else {
            $criteria = [
                'orderBy' => [
                    ['name', 'desc'],
                ],
            ];
            $telements = $this->storage->elementProvider()->all($criteria);
        }
        if ($telements) {
            foreach ($telements as $element) {
                $elements[$element->id] = $element;
            }
        }
        return $elements;
    }

    public function getParts()
    {
        return Arr::get($this->config, 'parts', ['value' => 'Value']);
    }
}
