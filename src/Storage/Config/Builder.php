<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/16/14
 * Time: 9:29 AM
 */

namespace Smorken\DynForm\Storage\Config;

use Illuminate\Support\Arr;

class Builder implements \Smorken\DynForm\Contracts\Storage\Builder
{

    protected $builders = [];

    protected $wrappers = [];

    public function __construct($builders, $wrappers)
    {
        $this->builders = $builders;
        $this->wrappers = $wrappers;
        ksort($this->builders);
        ksort($this->wrappers);
    }

    public function all()
    {
        return $this->builders;
    }

    public function allWrappers()
    {
        return $this->wrappers;
    }

    public function find($id)
    {
        $builders = $this->all();
        $clsname = $id ? Arr::get($builders, $id, null) : null;
        if ($clsname) {
            return new $clsname;
        }
        return null;
    }

    public function findWrapper($id, $view = null)
    {
        $wrappers = $this->allWrappers();
        $clsname = Arr::get($wrappers, $id, null);
        if ($clsname) {
            return new $clsname($view);
        }
        return null;
    }

    /**
     * $first_option can be null or in the form of array(key, value)
     *
     * @param  null|array  $first_option
     * @return array
     */
    public function getBuilderSelectList($first_option = null)
    {
        $list = [];
        if ($first_option) {
            $list[$first_option[0]] = $first_option[1];
        }
        $builders = $this->all();
        foreach ($builders as $k => $fullclass) {
            $c = new $fullclass;
            $add = '';
            if ($c->isContainer()) {
                $add = ' (container)';
            }
            $list[$k] = class_basename($fullclass).$add;
        }
        return $list;
    }

    public function getWrapperSelectList()
    {
        $list = [];
        $wrappers = $this->allWrappers();
        foreach ($wrappers as $k => $fullclass) {
            $list[$k] = class_basename($fullclass);
        }
        return $list;
    }
}
