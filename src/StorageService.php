<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 10:02 AM
 */

namespace Smorken\DynForm;

use Smorken\DynForm\Contracts\Handler\Storage;
use Smorken\DynForm\Contracts\Storage\Element;
use Smorken\DynForm\Contracts\Storage\Entry;
use Smorken\DynForm\Contracts\Storage\EntryData;
use Smorken\DynForm\Contracts\Storage\Form;
use Smorken\DynForm\Contracts\Storage\Lookup;

class StorageService implements Storage
{

    /**
     * @var EntryData
     */
    protected $dataProvider;

    /**
     * @var Element
     */
    protected $elementProvider;

    /**
     * @var Entry
     */
    protected $entryProvider;

    /**
     * @var Form
     */
    protected $formProvider;

    /**
     * @var Lookup
     */
    protected $lookupProvider;

    /**
     * @param  Form  $formProvider
     * @param  Element  $elementProvider
     * @param  Entry  $entryProvider
     * @param  EntryData  $dataProvider
     * @param  Lookup  $lookupProvider
     */
    public function __construct(
        Form $formProvider,
        Element $elementProvider,
        Entry $entryProvider,
        EntryData $dataProvider,
        Lookup $lookupProvider
    ) {
        $this->setFormProvider($formProvider);
        $this->setElementProvider($elementProvider);
        $this->setDataProvider($dataProvider);
        $this->setEntryProvider($entryProvider);
        $this->setLookupProvider($lookupProvider);
    }

    /**
     * @return EntryData
     */
    public function dataProvider()
    {
        return $this->dataProvider;
    }

    /**
     * @param  bool  $new
     * @return Element
     */
    public function elementProvider($new = false)
    {
        if ($new) {
            return clone $this->elementProvider;
        }
        return $this->elementProvider;
    }

    /**
     * @return Entry
     */
    public function entryProvider()
    {
        return $this->entryProvider;
    }

    /**
     * @param  bool  $new
     * @return Form
     */
    public function formProvider($new = false)
    {
        if ($new) {
            return clone $this->formProvider;
        }
        return $this->formProvider;
    }

    /**
     * @return Lookup
     */
    public function getLookupProvider()
    {
        return $this->lookupProvider;
    }

    /**
     * @param  Lookup  $lookupProvider
     */
    public function setLookupProvider($lookupProvider)
    {
        $this->lookupProvider = $lookupProvider;
    }

    /**
     * @param  EntryData  $dataProvider
     */
    public function setDataProvider($dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param  Element  $elementProvider
     */
    public function setElementProvider($elementProvider)
    {
        $this->elementProvider = $elementProvider;
    }

    /**
     * @param  Entry  $entryProvider
     */
    public function setEntryProvider($entryProvider)
    {
        $this->entryProvider = $entryProvider;
    }

    /**
     * @param  Form  $formProvider
     */
    public function setFormProvider($formProvider)
    {
        $this->formProvider = $formProvider;
    }
}
