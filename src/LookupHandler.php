<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/10/14
 * Time: 9:54 AM
 */

namespace Smorken\DynForm;

use Smorken\DynForm\Contracts\Handler\Lookup;

class LookupHandler implements Lookup
{

    protected $chunk_delimiter = ':';

    protected $errors = [];

    protected $param_delimiter = ',';

    protected $part_delimiter = ';';

    public function run($lookup)
    {
        $parts = explode($this->part_delimiter, $lookup);
        $handler = $this->parseIntoNamedArray($parts);
        if (!$this->hasErrors()) {
            $clsname = $handler['cls'];
            $method = $handler['method'];
            if ($handler['new']) {
                $o = [new $clsname, $method];
            } else {
                $o = $clsname.'::'.$method;
            }
            return call_user_func_array($o, $handler['params']);
        }
        return $this->errors;
    }

    protected function addError($msg)
    {
        $this->errors[] = $msg;
    }

    protected function convertToValue($chunk)
    {
        if (strtolower($chunk) === 'true') {
            return true;
        }
        if (strtolower($chunk) === 'false') {
            return false;
        }
        return $chunk;
    }

    protected function getErrors()
    {
        return $this->errors;
    }

    protected function hasErrors()
    {
        return count($this->errors);
    }

    protected function parseIntoNamedArray($parts)
    {
        $defaults = [
            'cls' => 'req',
            'method' => 'req',
            'new' => false,
            'params' => [],
        ];
        $parsed = [];
        foreach ($parts as $chunk) {
            $expChunk = explode($this->chunk_delimiter, $chunk);
            if (count($expChunk) == 2) {
                $value = $this->convertToValue($expChunk[1]);
                if (array_key_exists($expChunk[0], $defaults)) {
                    $parsed[$expChunk[0]] = $value;
                }
            }
        }
        foreach ($defaults as $k => $v) {
            if (!isset($parsed[$k]) || !$parsed[$k]) {
                if ($v === 'req') {
                    $this->addError("$k is a required lookup key.");
                    $parsed[$k] = null;
                } else {
                    $parsed[$k] = $v;
                }
            }
        }
        $parsed['params'] = $this->parseParameters($parsed['params'], $defaults['params']);
        return $parsed;
    }

    protected function parseParameters($parsed, $defaults)
    {
        if ($parsed == $defaults) {
            return $parsed;
        }
        return explode($this->param_delimiter, $parsed);
    }
}
