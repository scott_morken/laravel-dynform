<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 2:46 PM
 */

namespace Smorken\DynForm\Facade;

use Illuminate\Support\Facades\Facade;

class DynForm extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Smorken\DynForm\Contracts\Form::class;
    }
}
