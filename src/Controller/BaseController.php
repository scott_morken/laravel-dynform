<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 2:49 PM
 */

namespace Smorken\DynForm\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Smorken\DynForm\Contracts\Handler\Storage;
use Smorken\DynForm\Model\VO\Option;
use Smorken\DynForm\Model\VO\Options;

abstract class BaseController extends \Smorken\Ext\Controller\BaseController
{

    /**
     * @var Storage
     */
    protected $storageService;

    protected $subnav = 'admin';

    public function __construct(Storage $storageService)
    {
        $this->storageService = $storageService;
        parent::__construct();
    }

    abstract protected function addOptionsToResponse($response);

    public function optionAjax(Request $request)
    {
        if (!$request->wantsJson()) {
            abort(409, "Request method not allowed.");
        }
        $option = new Option(null, null, null);
        $v = $this->addOptionsToResponse($this->getSwitchable('_option', ['content' => $option]));
        return response()->make(
            [
                'element' => '#options-content',
                'content' => (string) $v->render(),
            ]
        );
    }

    public function storageService()
    {
        return $this->storageService;
    }

    protected function createArrayFromInput($input)
    {
        $arr = [];
        if ($input) {
            foreach ($input as $k => $values) {
                foreach ($values as $i => $v) {
                    $arr[$i][$k] = $v;
                }
            }
        }
        return $arr;
    }

    protected function getSwitchable($view, $view_data = [])
    {
        $v = $this->base_view.'.extended.'.$view;
        return View::make($v, $view_data);
    }

    protected function handleOptions($options)
    {
        $arr = $this->createArrayFromInput($options);
        $options = [];
        foreach ($arr as $i => $values) {
            [$key, $item, $value] = array_values($values);
            if ($key && strlen($value)) {
                $options[] = new Option($key, $item, $value);
            }
        }
        return new Options($options);
    }
}
