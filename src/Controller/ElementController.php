<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/15/14
 * Time: 7:49 AM
 */

namespace Smorken\DynForm\Controller;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Smorken\DynForm\Contracts\Handler\Storage;
use Smorken\DynForm\Contracts\Storage\Builder;
use Smorken\DynForm\Contracts\Storage\Element;
use Smorken\DynForm\FormRequest\ElementRequest;
use Smorken\DynForm\Model\VO\Data;
use Smorken\DynForm\Model\VO\Filter;
use Smorken\DynForm\Model\VO\Logic;
use Smorken\DynForm\Model\VO\Validator;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;

class ElementController extends BaseController
{

    use Full, IndexFiltered {
        create as traitedCreate;
        delete as traitedDelete;
        doDelete as traitedDoDelete;
        doSave as traitedDoSave;
        index as traitedIndex;
        update as traitedUpdate;
    }

    protected $base_view = 'smorken/dynform::controller.element';

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var \Smorken\DynForm\Contracts\Handler\Logic
     */
    protected $logic;

    /**
     * @var Element
     */
    protected $provider;

    public function __construct(
        Storage $storageService,
        Builder $builder,
        \Smorken\DynForm\Contracts\Handler\Logic $logic
    ) {
        $this->builder = $builder;
        $this->logic = $logic;
        $this->setProvider($storageService->elementProvider());
        parent::__construct($storageService);
    }

    public function create(Request $request)
    {
        return $this->addBuildersToResponse(
            $this->addLoookupsToResponse(
                $this->addOptionsToResponse($this->addLogicToResponse($this->traitedCreate($request)))
            ),
            false
        );
    }

    public function dataAjax(Request $request)
    {
        if (!$request->wantsJson()) {
            abort(409, "Request method not allowed.");
        }
        $v = $this->addLoookupsToResponse($this->getSwitchable('_datum', ['content' => null, 'key' => null]));
        return Response::json(
            [
                'element' => '#datum-content',
                'content' => (string) $v->render(),
            ]
        );
    }

    public function delete(Request $request, $id)
    {
        $model = $this->findModel($id);
        $resp = $this->deleteWithForms($request, $model, $id);
        if ($resp !== false) {
            return $resp;
        }
        return $this->traitedDelete($request, $id);
    }

    public function doSave(ElementRequest $request, $id = null)
    {
        return $this->traitedDoSave($request, $id);
    }

    public function index(Request $request)
    {
        return $this->addBuildersToResponse($this->traitedIndex($request));
    }

    public function logicAjax(Request $request)
    {
        if (!$request->wantsJson()) {
            abort(409, "Request method not allowed.");
        }
        $logic = new Logic();
        $v = $this->getSwitchable('_logic', ['content' => $logic, 'logic' => $this->logic->getProvider()]);
        return Response::json(
            [
                'element' => '#logic-content',
                'content' => (string) $v->render(),
            ]
        );
    }

    public function update(Request $request, $id)
    {
        return $this->addBuildersToResponse(
            $this->addLoookupsToResponse(
                $this->addOptionsToResponse(
                    $this->addLogicToResponse(
                        $this->traitedUpdate(
                            $request,
                            $id
                        )
                    )
                )
            ),
            false
        );
    }

    public function validatorAjax(Request $request)
    {
        if (!$request->wantsJson()) {
            abort(409, "Request method not allowed.");
        }
        $val = new Validator(null, null);
        $v = $this->getSwitchable('_validator', ['content' => $val]);
        return Response::json(
            [
                'element' => '#validators-content',
                'content' => (string) $v->render(),
            ]
        );
    }

    protected function addBuildersToResponse($response, $any = true)
    {
        $add = [];
        if ($any) {
            $add = ['*', 'Any Element'];
        }
        $builders = $this->builder->getBuilderSelectList($add);
        return $response->with('builders', $builders);
    }

    protected function addLogicToResponse($response)
    {
        return $response->with('logic', $this->logic->getProvider());
    }

    protected function addLoookupsToResponse($response, $type = 'data')
    {
        $lookups = $this->storageService->getLookupProvider()->allByType($type);
        return $response->with('lookups', $lookups);
    }

    protected function addOptionsToResponse($response)
    {
        $options = $this->storageService->elementProvider()->getOptionKeys();
        return $response->with('options', $options);
    }

    protected function deleteWithForms(Request $request, \Smorken\DynForm\Contracts\Model\Element $model, $id)
    {
        if ($model->forms && count($model->forms)) {
            $request->session()
                    ->flash('flash:danger', 'Element #'.$id.' belongs to one or more forms and cannot be deleted.');
            return Redirect::action($this->actionArray('index'));
        }
        return false;
    }

    protected function getAttributes(FormRequest $request)
    {
        return $this->getInput($request);
    }

    protected function getFilter(Request $r)
    {
        return new Filter(
            [
                'page' => $r->get('page', 1),
                'builder' => $r->get('builder'),
                'name' => $r->get('name', ''),
                'label' => $r->get('label', ''),
                'is_container' => $r->get('is_container'),
            ]
        );
    }

    protected function getInput(Request $request)
    {
        $input = $request->only(
            'name',
            'label',
            'builder',
            'is_container',
            'default',
            'help',
            'weight'
        );
        $input['options'] = $this->handleOptions($request->get('option', []));
        $input['validators'] = $this->handleValidators($request->get('validator', []));
        $input['data'] = $this->handleData($request->get('data', []));
        $input['logic'] = $this->handleLogic($request->get('logic', null));
        return $input;
    }

    protected function getParams($request)
    {
//        $filter = $this->getFilter($request);
//        return $filter->only(['builder']);
        return [];
    }

    protected function handleData($datas)
    {
        $data = new Data();
        $ds = $this->createArrayFromInput($datas);
        foreach ($ds as $i => $d) {
            if (strlen($d['value'])) {
                $data->addData($d['key'], $d['value']);
            }
        }
        return $data;
    }

    protected function handleLogic($logicarray)
    {
        $logic = null;
        if ($logicarray && array_key_exists('value', $logicarray) && strlen($logicarray['value']) && array_key_exists(
                'element',
                $logicarray
            )) {
            $logic = new Logic();
            $logic->action = $logicarray['action'];
            $logic->element = $logicarray['element'];
            $logic->part = $logicarray['part'];
            $logic->comparator = $logicarray['comparator'];
            $logic->value = $logicarray['value'];
        }
        return $logic;
    }

    protected function handleValidators($validators)
    {
        $ret = [];
        $vals = $this->createArrayFromInput($validators);
        foreach ($vals as $i => $val) {
            if ($val['validators']) {
                $ret[] = new Validator($val['validators'], $val['message']);
            }
        }
        return $ret;
    }
}
