<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/15/14
 * Time: 7:48 AM
 */

namespace Smorken\DynForm\Controller;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Smorken\DynForm\Contracts\Handler\Form;
use Smorken\DynForm\Contracts\Handler\Storage;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FormController extends BaseController
{

    use Full, IndexFiltered {
        create as traitedCreate;
        delete as traitedDelete;
        doDelete as traitedDoDelete;
        doSave as traitedDoSave;
        update as traitedUpdate;
    }

    protected $base_view = 'smorken/dynform::controller.form';

    /**
     * @var Form
     */
    protected $dynForm;

    /**
     * @var \Smorken\DynForm\Contracts\Storage\Form
     */
    protected $provider;

    public function __construct(Storage $storageService, Form $dynForm)
    {
        $this->dynForm = $dynForm;
        $this->setProvider($storageService->formProvider());
        parent::__construct($storageService);
    }

    public function create(Request $request)
    {
        return $this->addOptionsToResponse($this->traitedCreate($request));
    }

    public function delete(Request $request, $id)
    {
        $model = $this->findModel($id);
        $r = $this->deleteWithEntries($request, $model, $id);
        if ($r !== false) {
            return $r;
        }
        return $this->traitedDelete($request, $id);
    }

    public function deleteElement(Request $request, $id, $element_id)
    {
        $model = $this->findModel($id);
        $r = $this->getProvider()
                  ->detachElement($model, $element_id);
        if ($r) {
            $request->session()->flash('flash:success', "Removed element #$element_id from form #".$id);
        } else {
            $request->session()->flash('flash:danger', "Unable to remove element #$element_id from form!");
        }
        return Redirect::action($this->actionArray('elements'), ['id' => $id]);
    }

    public function doDelete(Request $request, $id)
    {
        $model = $this->findModel($id);
        $resp = $this->deleteWithEntries($request, $model, $id);
        if ($resp !== false) {
            return $resp;
        }
        $this->getProvider()
             ->detachAllElements($model);
        return $this->traitedDoDelete($request, $id);
    }

    public function doSave(\Smorken\DynForm\FormRequest\FormRequest $request, $id = null)
    {
        return $this->traitedDoSave($request, $id);
    }

    public function elements(Request $request, $id)
    {
        $model = $this->findModel($id);
        $containers = $this->storageService->formProvider()->getContainerSelectList($model);
        return View::make($this->getView('elements'))->with('model', $model)->with('containers', $containers);
    }

    public function postElements(Request $request, $id)
    {
        $model = $this->findModel($id);
        $element_id = $request->get('element_id');
        if ($element_id) {
            $element = $this->storageService()
                            ->elementProvider()
                            ->find($element_id);
            if (!$element) {
                throw new NotFoundHttpException("Resource [$element_id] not found.");
            }
            $input = $request->except('element_id', '_token');
            $ele = $this->getProvider()
                        ->addElement($model, $element, $input);
            if ($ele) {
                $request->session()->flash('flash:success', "Element added: ".$ele);
            } else {
                $request->session()->flash('flash:danger', "Error adding element.");
            }
        }
        return Redirect::action($this->actionArray('elements'), ['id' => $id]);
    }

    public function postPreview(Request $request, $id)
    {
        $model = $this->findModel($id);
        $v = $this->dynForm->validate($model, $request->all());
        if (!$v) {
            $request->session()->flash('flash:danger', 'Oh no, something went wrong!');
            return Redirect::action($this->actionArray('preview'), ['id' => $id])
                           ->withInput()
                           ->withErrors($this->dynForm->getErrors());
        } else {
            $request->session()->flash('flash:success', 'You would have successfully saved your data! Hooray!');
            return Redirect::action($this->actionArray('preview'), ['id' => $id])
                           ->withInput();
        }
    }

    public function postSaveOrderAjax(Request $request, $id)
    {
        $model = $this->findModel($id);
        if (!$request->wantsJson()) {
            abort(409, "Request method not allowed.");
        }
        $ordered = $request->get('ordered');
        $response = $this->getProvider()
                         ->updateOrder($model, $ordered);
        return Response::json(
            [
                'element' => '#order-response',
                'content' => $response,
            ]
        );
    }

    public function preview(Request $request, $id)
    {
        $model = $this->findModel($id);
        $data = $request->old() ?: [];
        return View::make($this->getView('preview'))
                   ->with('form', $this->dynForm)
                   ->with('model', $model)
                   ->with('data', $data)
                   ->with('raw', $request->get('raw') === '1');
    }

    public function update(Request $request, $id)
    {
        return $this->addOptionsToResponse($this->traitedUpdate($request, $id));
    }

    protected function addOptionsToResponse($response)
    {
        $options = $this->storageService->formProvider()->getOptionKeys();
        return $response->with('options', $options);
    }

    protected function deleteWithEntries(Request $request, \Smorken\DynForm\Contracts\Model\Form $model, $id)
    {
        if ($model->entries && count($model->entries)) {
            $request->session()->flash('flash:danger', 'Form #'.$id.' has one or more entries and cannot be deleted.');
            return Redirect::action($this->actionArray('index'));
        }
        return false;
    }

    protected function getAttributes(FormRequest $request)
    {
        $attributes = $request->only(['name']);
        $attributes['options'] = $this->handleOptions($request->get('option'));
        return $attributes;
    }

    protected function initStorageProvider()
    {
        $this->setProvider(
            $this->storageService()
                 ->formProvider()
        );
    }
}
