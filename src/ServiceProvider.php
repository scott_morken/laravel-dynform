<?php namespace Smorken\DynForm;

use Smorken\DynForm\Contracts\Handler\Form;
use Smorken\DynForm\Contracts\Handler\Logic;
use Smorken\DynForm\Contracts\Storage;
use Smorken\DynForm\Model\Eloquent\Element;
use Smorken\DynForm\Model\Eloquent\Entry;
use Smorken\DynForm\Model\Eloquent\EntryData;
use Smorken\DynForm\Model\Observer\ByAttributes;
use Smorken\DynForm\Storage\Config\Builder;
use Smorken\DynForm\Storage\Config\Lookup;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootFactory();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->bootViews();
        $this->bootConfig();
        $this->loadRoutes();
        $this->bootObservers();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Form::class,
            function ($app) {
                return new FormHandler(
                    $app['view'],
                    $app[Storage\Builder::class],
                    $app[Logic::class],
                    $app['config']->get('dynform.form_view', 'smorken/dynform::simple.container.form')
                );
            }
        );
        $this->app->bind(
            Logic::class,
            function ($app) {
                return new LogicHandler($app[Storage\Logic::class]);
            }
        );
        $this->bindFormProvider();
        $this->bindElementProvider();
        $this->bindEntryProvider();
        $this->bindDataProvider();
        $this->bindBuilderProvider();
        $this->bindLookupProvider();
        $this->bindLogicProvider();
        $this->app->singleton(
            Contracts\Handler\Storage::class,
            function ($app) {
                $fp = $app->make(Storage\Form::class);
                $fep = $app->make(Storage\Element::class);
                $ep = $app->make(Storage\Entry::class);
                $dp = $app->make(Storage\EntryData::class);
                $lp = $app->make(Storage\Lookup::class);
                return new StorageService($fp, $fep, $ep, $dp, $lp);
            }
        );
    }

    protected function bindBuilderProvider()
    {
        $this->app->bind(
            Storage\Builder::class,
            function ($app) {
                $builders = $app['config']->get('dynform.builders', []);
                $wrappers = $app['config']->get('dynform.wrappers', []);
                return new Builder($builders, $wrappers);
            }
        );
    }

    protected function bindDataProvider()
    {
        $this->app->bind(
            Storage\EntryData::class,
            function ($app) {
                $mclass = $app['config']->get('dynform.entry_data.model', EntryData::class);
                $m = new $mclass;
                $pclass = $app['config']->get('dynform.entry_data.storage', \Smorken\DynForm\Storage\Eloquent\EntryData::class);
                return new $pclass($m);
            }
        );
    }

    protected function bindElementProvider()
    {
        $this->app->bind(
            Storage\Element::class,
            function ($app) {
                $mclass = $app['config']->get('dynform.elements.model', Element::class);
                $m = new $mclass;
                $pclass = $app['config']->get('dynform.elements.storage', \Smorken\DynForm\Storage\Eloquent\Element::class);
                return new $pclass($m);
            }
        );
    }

    protected function bindEntryProvider()
    {
        $this->app->bind(
            Storage\Entry::class,
            function ($app) {
                $mclass = $app['config']->get('dynform.entries.model', Entry::class);
                $m = new $mclass;
                $pclass = $app['config']->get('dynform.entries.storage', \Smorken\DynForm\Storage\Eloquent\Entry::class);
                return new $pclass($m);
            }
        );
    }

    protected function bindFormProvider()
    {
        $this->app->bind(
            Storage\Form::class,
            function ($app) {
                $mclass = $app['config']->get('dynform.forms.model', \Smorken\DynForm\Model\Eloquent\Form::class);
                $m = new $mclass;
                $pclass = $app['config']->get('dynform.forms.storage', \Smorken\DynForm\Storage\Eloquent\Form::class);
                return new $pclass($m);
            }
        );
    }

    protected function bindLogicProvider()
    {
        $this->app->bind(
            Storage\Logic::class,
            function ($app) {
                $config = $app['config']->get('dynform.logic', []);
                $storage = $app[Contracts\Handler\Storage::class];
                return new \Smorken\DynForm\Storage\Config\Logic($config, $storage);
            }
        );
    }

    protected function bindLookupProvider()
    {
        $this->app->bind(
            Storage\Lookup::class,
            function ($app) {
                $lookups = $app['config']->get('dynform.lookups', []);
                return new Lookup($lookups);
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'dynform');
        $this->publishes([$config => config_path('dynform.php')], 'config');
    }

    protected function bootFactory()
    {
        if ($this->app->bound('Illuminate\Database\Eloquent\Factory') && class_exists('Faker\Generator')) {
            $this->app->make('Illuminate\Database\Eloquent\Factory')
                      ->load(__DIR__.'/../database/factories');
        }
    }

    protected function bootObservers()
    {
        $ep = $this->app->make(Storage\Entry::class);
        $model = $ep->getModel();
        $mclass = get_class($model);
        call_user_func([$mclass, 'observe'], new ByAttributes());
    }

    protected function bootViews()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'smorken/dynform');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('/views/vendor/smorken/dynform'),
            ],
            'views'
        );
    }

    protected function loadRoutes()
    {
        if ($this->app['config']->get('dynform.routes.load', true)) {
            require __DIR__.'/routes.php';
        }
    }
}
