<?php


namespace Smorken\DynForm\Contracts\Model;


use Illuminate\Support\Collection;
use Smorken\DynForm\Model\VO\Logic;
use Smorken\DynForm\Model\VO\Options;

interface Element
{

    public function addChild(Element $element);

    public function addMultiElement(Element $element);

    public function getAllElementAttributes($additional = []);

    public function getBuilder();

    public function getChildren();

    public function getContainer();

    public function getContainerId();

    public function getData();

    public function getDefault();

    public function getElementAttr($key);

    public function getElementAttributes($additional = []);

    public function getElementAttributesAsString($additional = []);

    public function getElementAttributesExcept(array $except, $additional = []);

    public function getErrors();

    public function getForm();

    public function getHelp();

    public function getId($append = null);

    public function getLabel();

    public function getLabelAttr($key);

    public function getLabelAttributes($additional = []);

    public function getLabelAttributesAsString($additional = []);

    public function getLogic();

    public function getModelId();

    public function getMultiElement($id);

    public function getMultiElements();

    public function getName();

    public function getOption($key);

    public function getOptions();

    public function getValidators();

    public function getValue();

    public function getWeight();

    public function getWrapper();

    public function getWrapperAttr($key);

    public function getWrapperAttributes($additional = []);

    public function getWrapperAttributesAsString($additional = []);

    public function hasErrors();

    public function hasLogic();

    public function hasWrapper();

    public function isCheckable();

    public function isContainer();

    public function setBuilder($builder);

    public function setChildren(Collection $children);

    public function setContainer($container);

    public function setData($data);

    public function setDefault($default);

    public function setElementAttr($key, $value);

    public function setElementAttributes($options);

    public function setForm(Form $form);

    public function setHelp($help);

    public function setId($id);

    public function setLabel($label);

    public function setLabelAttr($key, $value);

    public function setLabelAttributes($label_opts);

    public function setLogic(Logic $logic);

    public function setName($name);

    public function setOption($key, $item, $value);

    public function setOptions(Options $options);

    public function setValidators($validators);

    public function setValue($value);

    public function setWeight($weight);

    public function setWrapper($wrapper);

    public function setWrapperAttr($key, $value);

    public function setWrapperAttributes($options);

    public function validate($value = null, $data = []);
}
