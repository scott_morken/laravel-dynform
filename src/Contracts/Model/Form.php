<?php


namespace Smorken\DynForm\Contracts\Model;


use Illuminate\Support\Collection;
use Smorken\DynForm\Model\VO\Options;

interface Form
{

    public function getElements();

    public function getElementsNotAttached();

    public function getId();

    public function getModelId();

    public function getName();

    public function getOption($key);

    public function getOptionItems($key);

    public function getOptionKeys();

    public function getOptionValues($key, $item = null);

    /**
     * @return Options
     */
    public function getOptions();

    /**
     * @return Collection|Element[]
     */
    public function getStacked();

    public function setId($id);

    public function setName($name);

    /**
     * @param $key
     * @param $item  |null
     * @param $value
     */
    public function setOption($key, $item, $value);

    /**
     * @param  Options  $options
     */
    public function setOptions(Options $options);
}
