<?php


namespace Smorken\DynForm\Contracts\Model;

/**
 * Interface Entry
 * @package Smorken\DynForm\Contracts\Model
 *
 * @property integer $id
 * @property integer $form_id
 * @property integer $created_by
 * @property integer $updated_by
 *
 * Relations
 * @property Form $form
 * @property EntryData[] $data
 */
interface Entry
{

    public function dataKeyed($convert = false);
}
