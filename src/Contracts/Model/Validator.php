<?php


namespace Smorken\DynForm\Contracts\Model;

/**
 * Interface Validator
 * @package Smorken\DynForm\Contracts\Model
 *
 * @property string|null $message
 * @property mixed $validators
 */
interface Validator
{

    public function getValidator();

    public function setValidator($validators, $message = null);
}
