<?php


namespace Smorken\DynForm\Contracts\Model;

/**
 * Interface Logic
 * @package Smorken\DynForm\Contracts\Model
 *
 * @property string $action default 'show'
 * @property string $comparator default '='
 * @property Element $element
 * @property string $part default 'value'
 * @property mixed $value
 */
interface Logic
{

}
