<?php


namespace Smorken\DynForm\Contracts\Model;


interface Checkable
{

    /**
     * @return false|mixed
     */
    public function hiddenValue();

    /**
     * @param  bool  $default
     * @return boolean
     */
    public function isChecked($default = true);

    public function setChecked($checked = false);

    public function setHiddenValue($value = false);

    public function setIsCheckable($checkable = false);
}
