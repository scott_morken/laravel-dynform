<?php


namespace Smorken\DynForm\Contracts\Model;


interface Options
{

    public function addOption(Option $option);

    public function addOptions(array $options);

    public function getOption($key, $item = null);

    public function getOptions($toArray = true);

    public function toArray();
}
