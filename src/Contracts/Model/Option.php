<?php


namespace Smorken\DynForm\Contracts\Model;

/**
 * Interface Option
 * @package Smorken\DynForm\Contracts\Model
 *
 * @property string $item
 * @property string $key
 * @property mixed $value
 */
interface Option
{

    public function getKey();

    public function getValue();
}
