<?php


namespace Smorken\DynForm\Contracts\Model;


interface Data extends \Iterator
{

    public function addData($key, $value);

    public function all($expand = true);

    public function setData(array $data);
}
