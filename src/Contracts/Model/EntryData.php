<?php


namespace Smorken\DynForm\Contracts\Model;

/**
 * Interface EntryData
 * @package Smorken\DynForm\Contracts\Model
 *
 * @property integer $id
 * @property integer $entry_id
 * @property integer $element_id
 * @property string $value
 */
interface EntryData
{

    public function valueConvert();
}
