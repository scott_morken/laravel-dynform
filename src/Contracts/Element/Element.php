<?php


namespace Smorken\DynForm\Contracts\Element;


use Illuminate\Contracts\View\Factory;

interface Element
{

    /**
     * @return string
     */
    public function getViewBase();

    /**
     * @return string
     */
    public function getViewName();

    /**
     * @return bool
     */
    public function isContainer();

    /**
     * @param  \Smorken\DynForm\Contracts\Model\Element  $element
     * @param  Factory  $view
     * @param  null  $value
     * @return string
     */
    public function render(\Smorken\DynForm\Contracts\Model\Element $element, Factory $view, $value = null);

    /**
     * @param  string  $view_base
     * @return void
     */
    public function setViewBase($view_base);

    /**
     * @param  string  $view_name
     * @return void
     */
    public function setViewName($view_name);
}
