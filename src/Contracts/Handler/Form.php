<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 8:28 AM
 */

namespace Smorken\DynForm\Contracts\Handler;

interface Form
{

    public function create(\Smorken\DynForm\Contracts\Model\Form $form, $data = [], $disabled = false);

    public function getErrors();

    public function hasErrors();

    public function raw();

    public function render();

    public function setRawable($rawable = false);

    public function setRenderable($renderable = true);

    public function validate(\Smorken\DynForm\Contracts\Model\Form $form, array $data);

    public function walkOutput($outputArray = []);

    public function walkRaw($rawArray = []);
}
