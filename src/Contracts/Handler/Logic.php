<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 8:28 AM
 */

namespace Smorken\DynForm\Contracts\Handler;

use Illuminate\Contracts\View\Factory;

interface Logic
{

    public function addElement($element);

    public function createLogicView(Factory $view);

    public function getProvider();

    public function setElements($elements);
}
