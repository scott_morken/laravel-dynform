<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/7/18
 * Time: 8:28 AM
 */

namespace Smorken\DynForm\Contracts\Handler;

use Smorken\DynForm\Contracts\Storage\Element;
use Smorken\DynForm\Contracts\Storage\Entry;
use Smorken\DynForm\Contracts\Storage\EntryData;
use Smorken\DynForm\Contracts\Storage\Form;
use Smorken\DynForm\Contracts\Storage\Lookup;

interface Storage
{

    /**
     * @return EntryData
     */
    public function dataProvider();

    /**
     * @param  bool  $new
     * @return Element
     */
    public function elementProvider($new = false);

    /**
     * @return Entry
     */
    public function entryProvider();

    /**
     * @param  bool  $new
     * @return Form
     */
    public function formProvider($new = false);

    /**
     * @return Lookup
     */
    public function getLookupProvider();

    /**
     * @param  EntryData  $dataProvider
     */
    public function setDataProvider($dataProvider);

    /**
     * @param  Element  $elementProvider
     */
    public function setElementProvider($elementProvider);

    /**
     * @param  Entry  $entryProvider
     */
    public function setEntryProvider($entryProvider);

    /**
     * @param  Form  $formProvider
     */
    public function setFormProvider($formProvider);

    /**
     * @param  Lookup  $lookupProvider
     */
    public function setLookupProvider($lookupProvider);
}
