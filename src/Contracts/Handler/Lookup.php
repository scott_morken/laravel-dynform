<?php


namespace Smorken\DynForm\Contracts\Handler;


interface Lookup
{

    public function run($lookup);
}
