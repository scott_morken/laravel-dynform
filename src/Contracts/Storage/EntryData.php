<?php


namespace Smorken\DynForm\Contracts\Storage;


interface EntryData
{

    public function errors();

    public function saveData($form, $entry_id, $data = []);
}
