<?php


namespace Smorken\DynForm\Contracts\Storage;


interface Builder
{

    public function all();

    public function allWrappers();

    public function find($id);

    public function findWrapper($id);

    public function getBuilderSelectList($first_option = null);

    public function getWrapperSelectList();
}
