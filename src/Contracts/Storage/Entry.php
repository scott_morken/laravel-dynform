<?php


namespace Smorken\DynForm\Contracts\Storage;


interface Entry
{

    public function getDataKeyed($model);
}
