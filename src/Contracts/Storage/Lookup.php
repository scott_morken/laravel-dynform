<?php


namespace Smorken\DynForm\Contracts\Storage;


interface Lookup
{

    public function all();

    public function allByType($type);
}
