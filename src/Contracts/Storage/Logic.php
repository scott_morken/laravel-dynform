<?php


namespace Smorken\DynForm\Contracts\Storage;


interface Logic
{

    public function getActions();

    public function getBackend();

    public function getComparators();

    public function getElements($form);

    public function getParts();
}
