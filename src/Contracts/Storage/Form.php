<?php


namespace Smorken\DynForm\Contracts\Storage;


interface Form
{

    public function addElement($model, $element, $attributes);

    public function detachAllElements($model);

    public function detachElement($model, $element_id);

    public function getContainerSelectList($model);

    public function getOptionItems($key);

    public function getOptionKeys();

    public function getOptionValues($key, $item = null);

    public function updateOrder($model, $order = []);
}
