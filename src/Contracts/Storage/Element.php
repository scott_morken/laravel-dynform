<?php


namespace Smorken\DynForm\Contracts\Storage;


interface Element
{

    public function getOptionItems($key);

    public function getOptionKeys();

    public function getOptionValues($key, $item = null);
}
