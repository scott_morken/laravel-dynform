<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 2:15 PM
 */

namespace Smorken\DynForm\Element;

class Checkbox extends AbstractCheckedElement
{

    protected $type = 'checkbox';
}
