<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 2:03 PM
 */

namespace Smorken\DynForm\Element;

class Fieldset extends AbstractContainer
{

    protected $type = 'container.fieldset';
}
