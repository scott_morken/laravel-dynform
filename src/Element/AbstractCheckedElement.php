<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 2:05 PM
 */

namespace Smorken\DynForm\Element;

use Smorken\DynForm\Contracts\Model\Checkable;
use Smorken\DynForm\Contracts\Model\Element;

abstract class AbstractCheckedElement extends AbstractElement
{

    protected function getValueForId(Element $element, $value)
    {
        if ($this->has($value)) {
            return $value;
        }
        if ($element->getValue() !== false) {
            return $element->getValue();
        }
        return 'check';
    }

    protected function getViewData(\Smorken\DynForm\Contracts\Model\Element $element, $value = null)
    {
        if ($element instanceof Checkable && $element->hiddenValue() !== false) {
            $element->setId($element->getId($this->getValueForId($element, $value)));
        }
        return parent::getViewData($element, $value);
    }
}
