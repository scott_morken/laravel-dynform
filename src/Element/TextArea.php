<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 3:20 PM
 */

namespace Smorken\DynForm\Element;

class TextArea extends AbstractElement
{

    protected $type = 'textarea';
}
