<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 3:09 PM
 */

namespace Smorken\DynForm\Element;

class Radio extends AbstractMultiElement
{

    protected $singleValue = true;

    protected $type = 'radio';
}
