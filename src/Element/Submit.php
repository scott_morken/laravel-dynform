<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/7/14
 * Time: 8:02 AM
 */

namespace Smorken\DynForm\Element;

class Submit extends Button
{

    protected $type = 'submit';
}
