<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 12:49 PM
 */

namespace Smorken\DynForm\Element;

use Illuminate\Container\Container;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Smorken\DynForm\Contracts\Element\Element;
use Smorken\DynForm\Contracts\Model\Checkable;

/**
 * Class AbstractElement
 * @package Smorken\DynForm\Element
 * @property string $name
 * @property string $default
 * @property string $label
 * @property string $help
 * @property string $error
 * @property integer $weight;
 * @property array $options;
 * @property array $validators;
 * @property array $data;
 */
abstract class AbstractElement implements Element
{

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Repository
     */
    protected $config;

    protected $is_container = false;

    protected $type = 'dummy';

    protected $view_base;

    protected $view_base_default = 'smorken/dynform::simple';

    protected $view_name;

    public function __construct($view_name = null)
    {
        if ($view_name !== null) {
            $this->setViewName($view_name);
        }
    }

    public function getViewBase()
    {
        return $this->view_base ?: $this->getFromConfig('dynform.view_base', '');
    }

    public function setViewBase($view_base)
    {
        $this->view_base = $view_base;
    }

    public function getViewName()
    {
        return $this->view_name ?: $this->getFromConfig('dynform.view_name', 'simple');
    }

    public function setViewName($view_name)
    {
        $this->view_name = $view_name;
    }

    public function isContainer()
    {
        return $this->is_container;
    }

    /**
     * @param  \Smorken\DynForm\Contracts\Model\Element  $element
     * @param  Factory  $view
     * @param  null  $value
     * @return string
     */
    public function render(\Smorken\DynForm\Contracts\Model\Element $element, Factory $view, $value = null)
    {
        $data = $this->getViewData($element, $value);
        return $view->make($this->getView($element, $view), $data)->render();
    }

    protected function getApp()
    {
        if (!$this->app) {
            $this->app = Container::getInstance();
        }
        return $this->app;
    }

    protected function getFromConfig($key, $default = null)
    {
        if (!$this->config) {
            $this->config = $this->getApp()->make(Repository::class);
        }
        return $this->config->get($key, $default);
    }

    protected function getView(\Smorken\DynForm\Contracts\Model\Element $element, Factory $v)
    {
        $view = $element->getOption('view');
        if (!$view) {
            $view = $this->getViewName();
        }
        if ($v->exists($this->getViewBase().'.'.$this->type.'.'.$view)) {
            return $this->getViewBase().'.'.$this->type.'.'.$view;
        }
        return $this->view_base_default.'.'.$this->type.'.'.$view;
    }

    /**
     * @param  \Smorken\DynForm\Contracts\Model\Element  $element
     * @param  null  $value
     * @return array
     */
    protected function getViewData(\Smorken\DynForm\Contracts\Model\Element $element, $value = null)
    {
        if ($value !== null) {
            $element->setValue($value);
        }
        return ['element' => $element];
    }

    protected function has($value)
    {
        if (is_string($value)) {
            return strlen($value) > 0;
        }
        return (bool) $value;
    }
}
