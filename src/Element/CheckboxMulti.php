<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 2:33 PM
 */

namespace Smorken\DynForm\Element;

class CheckboxMulti extends AbstractMultiElement
{

    protected $type = 'checkboxmulti';
}
