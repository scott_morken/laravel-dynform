<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 12:19 PM
 */

namespace Smorken\DynForm\Element;

class Div extends AbstractContainer
{

    protected $type = 'container.div';
}
