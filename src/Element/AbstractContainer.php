<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 12:14 PM
 */

namespace Smorken\DynForm\Element;

class AbstractContainer extends AbstractElement
{

    protected $is_container = true;

    protected $type = 'container.div';
}
