<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/6/14
 * Time: 12:58 PM
 */

namespace Smorken\DynForm\Element;

class Text extends AbstractElement
{

    protected $type = 'text';

    protected $view_name = 'simple';
}
