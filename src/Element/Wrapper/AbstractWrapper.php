<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/13/14
 * Time: 8:07 AM
 */

namespace Smorken\DynForm\Element\Wrapper;

use Illuminate\Contracts\View\Factory;
use Smorken\DynForm\Contracts\Model\Element;
use Smorken\DynForm\Element\AbstractElement;

class AbstractWrapper extends AbstractElement
{

    protected $ext_type = 'div';

    protected $type = 'wrapper';

    protected function getView(Element $element, Factory $v)
    {
        $view = $element->getOption('view');
        if (!$view) {
            $view = $this->view_name;
        }
        if ($v->exists($this->view_base.'.'.$this->type.'.'.$this->ext_type.'.'.$view)) {
            return $this->view_base.'.'.$this->type.'.'.$this->ext_type.'.'.$view;
        }
        if ($v->exists($this->view_base.'.'.$this->type.'.'.$view)) {
            return $this->view_base.'.'.$this->type.'.'.$view;
        }
        return $this->view_base_default.'.'.$this->type.'.'.$view;
    }
}
