<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 12:51 PM
 */

namespace Smorken\DynForm\Element;

use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Smorken\DynForm\Contracts\Model\Element;
use Smorken\DynForm\Model\VO\ElementPlaceholder;

class AbstractMultiElement extends AbstractElement
{

    protected $checked = false;

    protected $increment = 1;

    protected $singleValue = false;

    /**
     * @param  Element  $element
     * @param  Factory  $view
     * @param  null  $value
     * @return string
     */
    public function render(Element $element, Factory $view, $value = null)
    {
        $count = 1;
        $data = $element->getData();
        $this->reset();
        foreach ($data as $svalue => $slabel) {
            $element->addMultiElement($this->cloneElement($element, $svalue, $slabel, $count, $value));
            $count++;
        }
        $this->handleChecked($element);
        return parent::render($element, $view, $value);
    }

    /**
     * @param $viewpath
     * @param  Element[]  $elements
     * @param  Factory  $view
     * @param  null  $value
     * @return string
     */
    protected function _render($viewpath, $elements, Factory $view, $value = null)
    {
        return $view->make($viewpath, ['elements' => $elements, 'value' => $value])->render();
    }

    protected function checkElementIsArray($element)
    {
        if (!$this->singleValue) {
            $name = $element->getName();
            if (!Str::endsWith($name, ']')) {
                $element->setName($name.'[]');
            }
        }
    }

    protected function cloneElement(Element $orig, $subval, $sublabel, $count, $value = null)
    {
        $this->checkElementIsArray($orig);
        $sub = new ElementPlaceholder();
        $sub->copy($orig);
        $sub->setId($orig->getId($this->ensureSubvalForId($subval)));
        $sub->setLabelAttr('for', $sub->getId());
        $sub->setLabel($sublabel);
        $sub->setValue($subval);
        $value = $this->getComparisonValue($orig, $value);
        if (($this->has($value) && $value == $subval) || (is_array($value) && in_array($subval, $value))) {
            $sub->setChecked(true);
            $this->checked = true;
        }
        return $sub;
    }

    protected function ensureSubvalForId($subval)
    {
        if (!$this->has($subval)) {
            $subval = $this->increment;
            $this->increment();
        }
        return $subval;
    }

    protected function getComparisonValue(Element $element, $value)
    {
        if ($this->has($value)) {
            return $value;
        }
        $default = $element->getDefault();
        if (!$this->checked && $this->has($default)) {
            return $default;
        }
        return null;
    }

    /**
     * Clear elements that are checked to the default value when an element(s) has
     * already been checked
     * @param  Element  $element
     */
    protected function handleChecked(Element $element)
    {
        if ($this->checked) {
            foreach ($element->getMultiElements() as $me) {
                if (!$me->isChecked(false)) {
                    $me->setChecked(false);
                }
            }
        }
    }

    protected function increment()
    {
        $this->increment++;
    }

    protected function reset()
    {
        $this->increment = 1;
    }
}
