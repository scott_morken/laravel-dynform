<?php


namespace Smorken\DynForm\Element;


use Illuminate\Contracts\View\Factory;

class Csrf extends AbstractElement
{

    protected $type = 'csrf';

    /**
     * @param  \Smorken\DynForm\Contracts\Model\Element  $element
     * @param  Factory  $view
     * @param  null  $value
     * @return string
     */
    public function render(\Smorken\DynForm\Contracts\Model\Element $element, Factory $view, $value = null)
    {
        return csrf_field();
    }
}
