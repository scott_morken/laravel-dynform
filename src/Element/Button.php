<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/7/14
 * Time: 8:04 AM
 */

namespace Smorken\DynForm\Element;

class Button extends AbstractElement
{

    protected $type = 'button';
}
