<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/14/14
 * Time: 1:59 PM
 */

namespace Smorken\DynForm\Element;

class Email extends AbstractElement
{

    protected $type = 'email';
}
