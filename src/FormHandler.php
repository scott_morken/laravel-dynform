<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/13/14
 * Time: 1:03 PM
 */

namespace Smorken\DynForm;

use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use InvalidArgumentException;
use Smorken\DynForm\Contracts\Handler\Logic;
use Smorken\DynForm\Contracts\Model\Element;
use Smorken\DynForm\Contracts\Model\Form;
use Smorken\DynForm\Contracts\Storage\Builder;

class FormHandler implements \Smorken\DynForm\Contracts\Handler\Form
{

    /**
     * @var Builder
     */
    protected $builderProvider;

    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * @var Form
     */
    protected $form;

    protected $form_view;

    /**
     * @var LogicHandler
     */
    protected $logicHandler;

    protected $output = [];

    protected $raw = [];

    protected $rawable = true;

    protected $renderable = true;

    /**
     * @var Factory
     */
    protected $view;

    public function __construct(
        Factory $view,
        Builder $builderProvider,
        Logic $logicHandler,
        $form_view = 'smorken/dynform::simple.container.form'
    ) {
        $this->view = $view;
        $this->builderProvider = $builderProvider;
        $this->logicHandler = $logicHandler;
        $this->errors = new MessageBag();
        $this->form_view = $form_view;
    }

    public function __toString()
    {
        return $this->render();
    }

    public function create(Form $form, $data = [], $disabled = false)
    {
        $this->view->share('builder', $this);
        $this->openElement($form, $this->form_view);
        $this->buildByType($form->getStacked(), $data, $disabled);
        $this->closeElement($form, $this->form_view);
        $logic = $this->logicHandler->createLogicView($this->view);
        if ($logic) {
            $this->addToOutput($logic->render());
            $this->addToRaw($logic);
        }
        return $this;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return !$this->errors->isEmpty();
    }

    public function raw()
    {
        return $this->walkRaw($this->raw);
    }

    public function render()
    {
        return $this->walkOutput($this->output);
    }

    public function setRawable($rawable = false)
    {
        $this->rawable = $rawable;
    }

    public function setRenderable($renderable = true)
    {
        $this->renderable = $renderable;
    }

    public function validate(Form $form, array $data)
    {
        $containers = $form->getStacked();
        foreach ($containers as $id => $container) {
            $this->validateElement($container, $data);
        }
        return !$this->hasErrors();
    }

    public function walkOutput($outputArray = [])
    {
        $out = '';
        if (!$outputArray || count($outputArray) === 0) {
            return $out;
        }
        foreach ($outputArray as $o) {
            if (is_array($o) || $o instanceof Collection) {
                $out .= $this->walkOutput($o);
            } else {
                $out .= (string) $o;
            }
        }
        return $out;
    }

    public function walkRaw($rawArray = [])
    {
        $out = [];
        if (!$rawArray || count($rawArray) === 0) {
            return $out;
        }
        foreach ($rawArray as $o) {
            if (is_array($o) || $o instanceof Collection) {
                $out[] = $this->walkRaw($o);
            } else {
                $out[] = $o;
            }
        }
        return $out;
    }

    protected function addContainer(Element $container, $data, $disabled)
    {
        $this->openContainer($container);
        $this->buildByType($container->getChildren(), $data, $disabled);
        $this->closeContainer($container);
    }

    protected function addElement(Element $element, $data, $disabled)
    {
        $this->logicHandler->addElement($element);
        if ($disabled) {
            $element->setElementAttr('disabled', 'disabled');
        }
        $value = $this->getValueFromData($element, $data);
        if ($data) {
            if (!$element->validate($value)) {
                $this->appendElementErrors($element->getName(), $element->getErrors());
            }
        }
        $this->openWrapper($element);
        $c = $this->lookupElementBuilder($element);
        $o = $this->getBuilderOutput($c, $element, $value);
        $this->addToOutput($o);
        $this->addToRaw($element);
        $this->closeWrapper($element);
    }

    protected function addToOutput($data)
    {
        if ($this->renderable) {
            $this->output[] = $data;
        }
    }

    protected function addToRaw($data)
    {
        if ($this->rawable) {
            $this->raw[] = $data;
        }
    }

    protected function addViewToOutput($model, $view)
    {
        $this->addToRaw($model);
        if ($this->renderable) {
            $this->addToOutput($this->view->make($view, ['element' => $model])->render());
        }
    }

    protected function appendElementErrors($name, $errors)
    {
        $this->errors->merge($errors);
    }

    protected function buildByType(Collection $elements, $data, $disabled)
    {
        foreach ($elements as $element) {
            if ($element->isContainer()) {
                $this->addContainer($element, $data, $disabled);
            } else {
                $this->addElement($element, $data, $disabled);
            }
        }
    }

    protected function closeContainer($container)
    {
        $this->doContainer('close', $container);
    }

    protected function closeElement($element, $view_base)
    {
        $this->addViewToOutput($element, $view_base.'.close');
    }

    protected function closeWrapper($element)
    {
        $this->doWrapper('close', $element);
    }

    protected function doContainer($which, $container)
    {
        if ($container->getBuilder()) {
            $cls = $this->lookupElementBuilder($container);
            $c = new $cls($which);
            $o = $this->getBuilderOutput($c, $container);
            $this->addToOutput($o);
            $this->addToRaw($container);
        }
    }

    protected function doWrapper($which, $element)
    {
        if ($element->hasWrapper()) {
            $cls = $this->lookupElementWrapper($element);
            $c = new $cls($which);
            $o = $this->getBuilderOutput($c, $element);
            $this->addToOutput($o);
            $this->addToRaw($element);
        }
    }

    protected function getBuilderOutput($builder, $model, $value = null)
    {
        return $builder->render($model, $this->view, $value);
    }

    protected function getValueFromData(Element $element, $data)
    {
        $name = $element->getName();
        if ($name) {
            return Arr::get($data, $name, null);
        }
        return null;
    }

    protected function lookupElementBuilder(Element $model)
    {
        $builder = $model->getBuilder();
        $cls = $this->builderProvider->find($builder);
        if (!$cls) {
            throw new InvalidArgumentException("$builder does not exist.");
        }
        return $cls;
    }

    protected function lookupElementWrapper(Element $model)
    {
        $wrapper = $model->getWrapper();
        $cls = $this->builderProvider->findWrapper($wrapper);
        if (!$cls) {
            throw new InvalidArgumentException("$wrapper does not exist.");
        }
        return $cls;
    }

    protected function openContainer($container)
    {
        $this->doContainer('open', $container);
    }

    protected function openElement($element, $view_base)
    {
        $this->addViewToOutput($element, $view_base.'.open');
    }

    protected function openWrapper($element)
    {
        $this->doWrapper('open', $element);
    }

    protected function validateElement(Element $element, array $data)
    {
        if ($element->isContainer()) {
            return $this->validateElements($element->getChildren(), $data);
        } else {
            $value = $this->getValueFromData($element, $data);
            if (!$element->validate($value, $data)) {
                $this->appendElementErrors($element->getName(), $element->getErrors());
                return false;
            }
        }
        return true;
    }

    protected function validateElements(Collection $elements, array $data)
    {
        $valid = true;
        foreach ($elements as $element) {
            $v = $this->validateElement($element, $data);
            if ($v === false) {
                $valid = false;
            }
        }
        return $valid;
    }
}
