<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/15/14
 * Time: 7:57 AM
 */

use Smorken\DynForm\Controller\ElementController;
use Smorken\DynForm\Controller\FormController;

Route::group(
    [
        'middleware' => ['web', 'auth', 'can:role-admin'],
        'prefix' => config('dynform.route_prefix', 'admin/dynform'),
    ],
    function () {
        Route::group(
            [
                'prefix' => 'form',
            ],
            function () {
                $controller = config('dynform.forms.controller', FormController::class);
                \Smorken\Support\Routes::create($controller, [], [
                    'get|delete-element/{id}/{element_id}' => 'deleteElement',
                    'get|elements/{id}' => 'elements',
                    'post|elements/{id}' => 'postElements',
                    'get|preview/{id}' => 'preview',
                    'post|preview/{id}' => 'postPreview',
                    'post|save-order-ajax/{id}' => 'postSaveOrderAjax',
                    'get|options-ajax' => 'optionAjax',
                ]);
            }
        );

        Route::group(
            [
                'prefix' => 'element',
            ],
            function () {
                $controller = config('dynform.elements.controller',
                    ElementController::class);
                \Smorken\Support\Routes::create($controller, [], [
                    'get|data-ajax' => 'dataAjax',
                    'get|logic-ajax' => 'logicAjax',
                    'get|validator-ajax' => 'validatorAjax',
                    'get|option-ajax' => 'optionAjax',
                ]);
            }
        );
    }
);
