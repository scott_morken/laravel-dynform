<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/12/14
 * Time: 12:04 PM
 */

namespace Smorken\DynForm;

use Illuminate\Contracts\View\Factory;
use Smorken\DynForm\Contracts\Handler\Logic;

class LogicHandler implements Logic
{

    protected $elements = [];

    protected $has_logic = false;

    protected $logic = [];

    protected $logicProvider;

    public function __construct(\Smorken\DynForm\Contracts\Storage\Logic $logicProvider)
    {
        $this->logicProvider = $logicProvider;
    }

    public function addElement($element)
    {
        $this->elements[] = $element;
    }

    public function createLogicView(Factory $view)
    {
        $this->buildLogicArray();
        $view_path = 'smorken/dynform::logic.'.$this->logicProvider->getBackend().'.wrapperjs';
        if ($this->has_logic) {
            $view_data = ['logics' => $this->logic];
            return $view->make($view_path, $view_data);
        }
    }

    public function getProvider()
    {
        return $this->logicProvider;
    }

    public function setElements($elements)
    {
        $this->elements = $elements;
    }

    protected function buildLogicArray()
    {
        foreach ($this->elements as $element) {
            if ($element->hasLogic()) {
                $this->has_logic = true;
                $logic = $element->getLogic();
                $this->logic[$logic->element][$element->getId()] = $logic;
            }
        }
    }
}
