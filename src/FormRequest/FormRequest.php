<?php


namespace Smorken\DynForm\FormRequest;


use Smorken\DynForm\Contracts\Storage\Form;

class FormRequest extends \Illuminate\Foundation\Http\FormRequest
{

    public function rules(Form $form)
    {
        return $form->validationRules();
    }
}
