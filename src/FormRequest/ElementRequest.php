<?php


namespace Smorken\DynForm\FormRequest;


use Smorken\DynForm\Contracts\Storage\Element;

class ElementRequest extends \Illuminate\Foundation\Http\FormRequest
{

    public function rules(Element $element)
    {
        return $element->validationRules();
    }

    public function validator(Element $element, $factory)
    {
        $this->ensureAttributes();
        return $factory->make(
            $this->validationData(),
            $this->rules($element),
            $this->messages(),
            $this->attributes()
        );
    }

    protected function ensureAttributes()
    {
        $required = ['weight' => 0];
        $input = $this->all();
        foreach ($required as $k => $v) {
            if (is_null($this->get($k))) {
                $input[$k] = $v;
            }
        }
        $this->replace($input);
    }
}
