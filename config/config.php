<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/13/14
 * Time: 12:51 PM
 */
return [
    /*
     * table prefixes
     * make sure to set before running the migrations
     */
    'prefix' => null,

    /*
     * the view name injected into the element builder
     * unless overriden
     */
    'view_name' => 'simple',

    /*
     * the base view directory
     */
    'view_base' => 'smorken/dynform::bootstrap',

    /*
     * the master layout for the controller views
     */
    'view_master' => 'layouts.app',

    /*
     * the view directory for the open and close views for the form
     */
    'form_view' => 'smorken/dynform::simple.container.form',

    /*
     * the element builders, these will be used in the form_elements table and
     * shown in the builders dropdowns
     */
    'builders' => [
        'button' => \Smorken\DynForm\Element\Button::class,
        'checkbox' => \Smorken\DynForm\Element\Checkbox::class,
        'checkboxmulti' => \Smorken\DynForm\Element\CheckboxMulti::class,
        'csrf' => \Smorken\DynForm\Element\Csrf::class,
        'date' => \Smorken\DynForm\Element\Date::class,
        'div' => \Smorken\DynForm\Element\Div::class,
        'email' => \Smorken\DynForm\Element\Email::class,
        'fieldset' => \Smorken\DynForm\Element\Fieldset::class,
        'number' => \Smorken\DynForm\Element\Number::class,
        'radio' => \Smorken\DynForm\Element\Radio::class,
        'select' => \Smorken\DynForm\Element\Select::class,
        'submit' => \Smorken\DynForm\Element\Submit::class,
        'text' => \Smorken\DynForm\Element\Text::class,
        'textarea' => \Smorken\DynForm\Element\TextArea::class,
    ],

    /*
     * the element wrappers, these will be used to create the wrappers around
     * elements if selected
     */
    'wrappers' => [
        'div' => \Smorken\DynForm\Element\Wrapper\Div::class,
        'span' => \Smorken\DynForm\Element\Wrapper\Span::class,
        'p' => \Smorken\DynForm\Element\Wrapper\P::class,
    ],

    /*
     * the route prefix for the form and form element controllers
     */
    'route_prefix' => 'admin/dynform',

    /*
     * the classes responsible for the forms controller, model and storage
     */
    'forms' => [
        'controller' => \Smorken\DynForm\Controller\FormController::class,
        'model' => \Smorken\DynForm\Model\Eloquent\Form::class,
        'storage' => \Smorken\DynForm\Storage\Eloquent\Form::class,
    ],

    /*
     * the classes responsible for the form elements controller, model and storage
     */
    'elements' => [
        'controller' => \Smorken\DynForm\Controller\ElementController::class,
        'model' => \Smorken\DynForm\Model\Eloquent\Element::class,
        'storage' => \Smorken\DynForm\Storage\Eloquent\Element::class,
    ],

    /**
     * the classes responsible for the entry model and storage
     */
    'entries' => [
        'model' => \Smorken\DynForm\Model\Eloquent\Entry::class,
        'storage' => \Smorken\DynForm\Storage\Eloquent\Entry::class,
    ],

    /**
     * the classes responsible for the entry data model and storage
     */
    'entry_data' => [
        'model' => \Smorken\DynForm\Model\Eloquent\EntryData::class,
        'storage' => \Smorken\DynForm\Storage\Eloquent\EntryData::class,
    ],

    'logic' => [
        'backend' => 'jquery',
        'actions' => [
            'show' => 'Show',
            'hide' => 'Hide',
        ],
        'parts' => [
            'value' => 'Value',
        ],
        'comparators' => [
            '=' => 'Equal to',
            '<>' => 'Not equal to',
            '<' => 'Less than',
            '<=' => 'Less than or equal to',
            '>' => 'Greater than',
            '>=' => 'Greater than or equal to',
        ],
    ],
    'lookups' => [
        /*
     * data providers for data option
     * cls and method are required
     * ** params are always evaluated as strings
     * array(
     *   "cls:\\ClsName;new:false;method:myMethod;params:param1,param2" => 'My lookup',
     *   ...
     * )
     */
        'data' => [
        ],
    ],
];
