<?php

$factory->define(
    \Smorken\DynForm\Model\Eloquent\Entry::class,
    function (Faker\Generator $faker) {
        $user = $faker->randomNumber(3);
        return [
            'form_id' => $faker->randomNumber(3),
            'created_by' => $user,
            'updated_by' => $user,
        ];
    }
);

$factory->define(
    \Smorken\DynForm\Model\Eloquent\EntryData::class,
    function (Faker\Generator $faker) {
        return [
            'entry_id' => $faker->randomNumber(3),
            'element_id' => $faker->randomNumber(3),
            'value' => $faker->randomElement(['foo', 'bar', 1, 0]),
        ];
    }
);

$factory->define(
    \Smorken\DynForm\Model\Eloquent\Form::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->words(3, true),
        ];
    }
);

$factory->define(
    \Smorken\DynForm\Model\Eloquent\Element::class,
    function (Faker\Generator $faker) {
        return [
            'builder' => $faker->randomElement(
                [
                    'button',
                    'checkbox',
                    'date',
                    'email',
                    'number',
                    'submit',
                    'text',
                    'textarea',
                ]
            ),
            'name' => \Illuminate\Support\Str::random(10),
            'label' => $faker->words(3, true),
            'default' => $faker->randomElement([null, 1]),
            'help' => $faker->randomElement([null, $faker->words(5, true)]),
            'weight' => $faker->randomNumber(1),
            'is_container' => 0,
        ];
    }
);
