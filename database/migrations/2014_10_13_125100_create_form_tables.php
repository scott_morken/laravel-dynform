<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormTables extends Base
{

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['forms', 'elements', 'element_form'];
        foreach ($tables as $t) {
            Schema::dropIfExists($this->getTableName($t));
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->getTableName('forms'), function (Blueprint $t) {
            $t->increments('id');
            $t->string('name', 64);
            $t->text('options')->nullable();
            $t->timestamps();
        });

        Schema::create($this->getTableName('elements'), function (Blueprint $t) {
            $t->increments('id');
            $t->boolean('is_container')->default(0);
            $t->string('builder', 128);
            $t->string('name', 64);
            $t->string('label', 128)->nullable();
            $t->string('default', 64)->nullable();
            $t->string('help')->nullable();
            $t->text('validators')->nullable();
            $t->text('options')->nullable();
            $t->text('data')->nullable();
            $t->tinyInteger('weight')->default(0);
            $t->text('logic')->nullable();
            $t->timestamps();

            $t->index('weight', 'e_weight_ndx');
        });

        Schema::create($this->getTableName('element_form'), function (Blueprint $t) {
            $t->increments('id');
            $t->integer('form_id')->unsigned();
            $t->integer('element_id')->unsigned();
            $t->tinyInteger('weight')->default(0);
            $t->integer('container_id')->nullable()->unsigned();

            $t->index('form_id', 'fem_form_id_ndx');
            $t->index('element_id', 'fem_element_id_ndx');
            $t->index('weight', 'fem_weight_ndx');
            $t->index('container_id', 'fem_container_id');
        });

    }

}
