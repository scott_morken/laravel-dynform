<?php

use Illuminate\Database\Migrations\Migration;

class Base extends Migration {

    protected function getPrefix()
    {
        $p = \Config::get('smorken/dynform::config.prefix', null);
        return $p;
    }

    protected function getTableName($name)
    {
        $table = '';
        $p = $this->getPrefix();
        if ($p) {
            $table = $p . '_';
        }
        $table .= $name;
        return $table;
    }
} 