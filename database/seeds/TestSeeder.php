<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/9/16
 * Time: 9:32 AM
 */
class TestSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {

    }

    protected function seedFormsAndElements()
    {
        factory(\Smorken\DynForm\Model\Eloquent\Form::class, 5)->create()
                                                               ->each(
                                                                   function ($f) {
                                                                       $num = rand(3, 5);
                                                                       $f->elements()->saveMany(
                                                                           factory(\Smorken\DynForm\Model\Eloquent\Element::class,
                                                                               $num)->make(),
                                                                           ['container_id' => 0]
                                                                       );
                                                                   }
                                                               );
    }
}
